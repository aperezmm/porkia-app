/** @type {import('tailwindcss').Config} */

module.exports = {
  content: [
    './src/**/*.{js,jsx,ts,tsx}', 
    './index.html'],
  theme: {
    extend: {
      transitionDuration: {
        '0': '0ms',
        '100': '100ms',
        '200': '200ms',
        '300': '300ms',
        '400': '400ms',
        '500': '500ms',
        '600': '600ms',
        '700': '700ms',
        '800': '800ms',
        '900': '900ms',
        '1000': '1000ms',
        '1500': '1500ms',
        '2000': '2000ms',
      }
    },
    screens: {
      xxs: '0px',  // 0   - 359
      xs: '360px', // 360 - 599
      sm: '600px', // 600 - 959
      md: '960px', // 960 - 1279
      lg: '1280px',// 1280 - 1599
      xl: '1600px',// 1600 - 1919
      "2xl": '1920px', // 1920 ....
      print: { 'raw': 'print' }
    },
    colors: {
      primary: "#706FE5",
      secondary: "#EAEAFC",
      footer: "#2E3E5C",
      'gray-200': '#E5E7EB',

    }
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
