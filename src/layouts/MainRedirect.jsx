import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";

//Services
import TokenService from "@/services/TokenService";

export const MainRedirect = () => {

    const navigate = useNavigate();
    const [tokenService] = useState(TokenService());

    const [defaultRedirectByRole] = useState({
        ADMIN: "/main/admin",
        USER: "/main/user"
    })

    useEffect(() => {
        const token = tokenService.getToken();
        //Prevent the token exists
        if(!token){
            //move the user to login
            navigate('/login')
        } else {
            const userRole = tokenService.getUserRole();

            let redirectRoute = defaultRedirectByRole[userRole];
            //move the user to respective main
            navigate(redirectRoute);
        }
    },[])

    return (
        <div className="flex items-center justify-center h-screen">
            <h2>Redirigiendo...</h2>
        </div>
    )

}