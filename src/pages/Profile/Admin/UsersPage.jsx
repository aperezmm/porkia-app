import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
//components
import { TableComponent } from '@/components/TableDynamic';
import { DialogComponent } from '@/components/User/DialogComponent';
import { SweetAlert } from '@/components/Alerts/SweetAlert';
//services
import {
    getUsers,
    updateUserInformation,
    updateUserPassword
} from '@/services/User/UserManagement';
import { getInputs } from '@/utils/getInputs';
//Services
import { SignUpService } from '@/services/Auth/AuthManagement';
//hooks
import { useUser } from '@/hooks/useUser';
import { useDebounce } from '@/hooks/useDebounce';
//material ui
import { TextField, InputAdornment } from '@mui/material';
import { Search } from '@mui/icons-material';
import { SweetAlertConfirm } from '@/components/Alerts/SweetAlertConfirm';
import PersonAddAltRoundedIcon from '@mui/icons-material/PersonAddAltRounded';
import { LoadingTextComponent } from '@/components/LoadingTextComponent';

const columns = [
    { key: 'name', label: 'Nombre' },
    { key: 'surname', label: 'Apellido' },
    { key: 'email', label: 'Email' },
    { key: 'cellphone', label: 'Celular' }
];

export const UsersPage = () => {
    //custom hook
    const { tokenValue, isAdmin, isTokenExpired, userInformation } = useUser();

    const navigate = useNavigate();
    const [userList, setUserList] = useState(null);
    //forms formik states
    const [inputsForm, setInputsForm] = useState({});

    //dialog states
    const [openDialog, setOpenDialog] = useState(false);
    const [dialogTitle, setDialogTitle] = useState('');
    const [dialogFormLabel, setDialogFormLabel] = useState('');
    const [dialogUser, setDialogUser] = useState(null);
    const [keyForm, setKeyForm] = useState('');
    const [isFetchingLoading, setIsFetchingLoading] = useState(false);

    //control the user keyword
    const [filterText, setFilterText] = useState('');
    const debouncedSearchUser = useDebounce(filterText, 500);

    const handleEditTable = (user) => {
        // Aquí podrías abrir un modal o hacer cualquier otra acción para editar la fila seleccionada
        let formikConfig = getInputs('updateUser');

        formikConfig = {
            ...formikConfig,
            initialValues: {
                ...formikConfig.initialValues,
                email: user.email,
                username: user.email,
                name: user.name,
                surname: user.surname,
                cellphone: user.cellphone,
                country: user.country,
                about_me: user.about_me
            }
        };

        setInputsForm(formikConfig);
        setDialogUser(user);
        setDialogTitle('Actualizar información de usuario');
        setDialogFormLabel('Actualizar');
        setKeyForm('update_user_info');
        setOpenDialog(true);
    };

    const handleChangePasswordTable = (user) => {
        setInputsForm(getInputs('fotgotPassword'));
        setDialogTitle('Cambiar contraseña del usuario');
        setDialogUser(user);
        setDialogFormLabel('Actualizar contraseña');
        setKeyForm('update_user_password');
        setOpenDialog(true);
    };

    const handleRegisterNewUser = () => {
        setInputsForm(getInputs('registerUser'));
        setDialogTitle('Registrar nuevo usuario');
        setDialogFormLabel('Registrar usuario');
        setKeyForm('register_user');
        setOpenDialog(true);
    };

    const checkPassword = (password) => {
        const regex = /^(?=.*[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?])(?=.*[A-Z]).{8,}$/;
        return regex.test(password);
    };

    const signUpUser = (data) => {
        setIsFetchingLoading(true);
        SignUpService({ data, tokenValue }).then((response) => {
            const { status, data } = response;
            setIsFetchingLoading(false);
            if (status === 200) {
                getAllUsers();
                SweetAlert({ title: '¡Bien hecho!', text: data.message, icon: 'success' });
                setOpenDialog(false);
            } else {
                SweetAlert({
                    title: '¡Atención!',
                    text: data.message || 'Ha ocurrido un error en el servicio',
                    icon: 'error'
                });
            }
        });
    };

    const updatePasswordUser = (data) => {
        setIsFetchingLoading(true);
        updateUserPassword({ data, tokenValue }).then((response) => {
            const { status, data } = response;
            setIsFetchingLoading(false);
            if (status === 200) {
                SweetAlert({ title: '¡Bien hecho!', text: data.message, icon: 'success' });
                setOpenDialog(false);
            } else {
                SweetAlert({
                    title: '¡Atención!',
                    text: data.message || 'Ha ocurrido un error en el servicio',
                    icon: 'error'
                });
            }
        });
    };

    const handleEditUser = (form) => {
        switch (keyForm) {
            case 'register_user':
                //parsing the roles to array
                let newData = {
                    ...form,
                    roles: [form.roles]
                };

                if (!checkPassword(newData.password)) {
                    SweetAlertConfirm({
                        title: '¡Atención contraseña!',
                        text: 'Se sugiere una contraseña que contenga mínimo un carácter especial, una letra mayúscula y mínimo de 8 caracteres de longitud. ¿Desea ignorar y registrar?',
                        confirmButtonText: 'Realizar registro',
                        cancelButtonText: 'Cancelar',
                        icon: 'warning'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            //Continue with the register
                            signUpUser(newData);
                        }
                    });
                } else {
                    //Continue with the register
                    signUpUser(newData);
                }
                break;

            case 'update_user_info':
                setIsFetchingLoading(true);
                updateUserInformation({ data: form, tokenValue, userId: dialogUser._id }).then(
                    (response) => {
                        const { status, data } = response;
                        setIsFetchingLoading(false);
                        if (status === 200) {
                            //TODO:
                            getAllUsers(); //No hacer este llamado, actualizar el objeto con la respuesta
                            SweetAlert({
                                title: '¡Bien hecho!',
                                text: data.message,
                                icon: 'success'
                            });
                            setOpenDialog(false);
                        }
                    }
                );
                break;

            case 'update_user_password':
                //add other params into data
                let formData = {
                    ...form,
                    userId: dialogUser._id,
                    userIdentification: dialogUser.identification
                };

                if (!checkPassword(formData.password)) {
                    SweetAlertConfirm({
                        title: '¡Atención contraseña!',
                        text: 'Se sugiere una contraseña que contenga mínimo un carácter especial, una letra mayúscula y mínimo de 8 caracteres de longitud. ¿Desea ignorar y actualizar?',
                        confirmButtonText: 'Actualizar contraseña',
                        cancelButtonText: 'Cancelar',
                        icon: 'warning'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            //Continue with the register
                            updatePasswordUser(formData);
                        }
                    });
                } else {
                    //Continue with the register
                    updatePasswordUser(formData);
                }
                break;

            default:
                break;
        }
    };

    const getAllUsers = () => {
        getUsers().then((response) => {
            const { status, data } = response;

            if (status === 200) {
                const { docs } = data;
                setUserList(docs);
            } else {
                setUserList([]);
            }
        });
    };

    const getUsersSearchKey = (keyword) => {
        getUsers({ keyword, tokenValue }).then((response) => {
            const { status, data } = response;
            if (status === 200) {
                const { docs } = data;
                setUserList(docs);
            } else {
                setUserList([]);
            }
        });
    };

    const handleFilterChange = (event) => {
        setFilterText(event.target.value);
    };

    useEffect(() => {
        if (debouncedSearchUser) {
            getUsersSearchKey(debouncedSearchUser);
        } else {
            getAllUsers();
        }
    }, [debouncedSearchUser]);

    return (
        <div id="users-page-admin" className="container-appbar">
            <div className="container-card" id="users-page-container-card">
                <div className="m-3">
                    <h1 className="text-lg font-semibold">Administrador, </h1>
                    <h1>
                        Acá puedes registrar usuarios, actualizar información o editar contraseña de
                        el usuario que elijas!
                    </h1>
                </div>
                <div className="m-3"  id="users-page-admin-actions">
                    <button className="mt-2 blue-button" onClick={() => handleRegisterNewUser()}>
                        <PersonAddAltRoundedIcon className="mr-2" />
                        Registrar usuario
                    </button>
                </div>
                {(userList && userList.length) > 0 ? (
                    <>
                        <div className="flex justify-center general-font m-3">
                            <TextField
                                className="text-field-search general-font"
                                label="Buscar usuario"
                                value={filterText}
                                onChange={handleFilterChange}
                                InputProps={{
                                    startAdornment: (
                                        <InputAdornment position="start">
                                            <Search />
                                        </InputAdornment>
                                    )
                                }}
                                helperText="Buscar por cédula, nombres y apellidos"
                            />
                        </div>

                        <TableComponent
                            id="users-page-admin-table"
                            data={userList}
                            columns={columns}
                            onUserEdit={handleEditTable}
                            onUserChangePassword={handleChangePasswordTable}
                        />
                    </>
                ) : userList?.length === 0 ? (
                    <div id="farms-page-message">
                        <h1 className="text-lg text-center font-bold m-3">
                            No hay usuarios 🌾!
                        </h1>
                    </div>
                ) : (
                    <LoadingTextComponent />
                )}

                <DialogComponent
                    inputsForm={inputsForm}
                    openDialog={openDialog}
                    dialogTitle={dialogTitle}
                    onCloseDialog={() => setOpenDialog(false)}
                    labelForm={dialogFormLabel}
                    onSubmit={handleEditUser}
                    disabledButton={isFetchingLoading}
                />
            </div>
        </div>
    );
};
