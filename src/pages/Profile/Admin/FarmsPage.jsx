import { useEffect, useState } from 'react';

//material ui
import { TextField, InputAdornment } from '@mui/material';
import { Search } from '@mui/icons-material';
import { TableComponent } from '@/components/TableDynamic';
import { getFarms } from '@/services/Farm/FarmManagement';
import AgricultureRoundedIcon from '@mui/icons-material/AgricultureRounded';

//hooks
import { useUser } from '@/hooks/useUser';
import { useDebounce } from '@/hooks/useDebounce';
import { getInputs } from '@/utils/getInputs';
import { DialogComponent } from '@/components/Farms/DialogComponent';
import { getUsers } from '@/services/User/UserManagement';
import { createFarmAndAssignFarmer } from '@/services/Farm/FarmManagement';
import { SweetAlert } from '@/components/Alerts/SweetAlert';
import { LoadingTextComponent } from '@/components/LoadingTextComponent';

const columns = [
    { key: 'name', label: 'Nombre' },
    { key: 'farmerName', label: 'Administrador' },
    { key: 'country', label: 'Ciudad' }
];

export const FarmsPage = () => {
    const { tokenValue, isAdmin, isTokenExpired, userInformation } = useUser();

    //forms formik states
    const [inputsForm, setInputsForm] = useState({});

    //dialog states
    const [openDialog, setOpenDialog] = useState(false);
    const [dialogTitle, setDialogTitle] = useState('');
    const [dialogFormLabel, setDialogFormLabel] = useState('');
    const [dialogUser, setDialogUser] = useState(null);
    const [keyForm, setKeyForm] = useState('');

    const [farmList, setFarmList] = useState(null);
    const [userList, setUserList] = useState([]);

    //control the user keyword
    const [filterText, setFilterText] = useState('');
    const debouncedSearchUser = useDebounce(filterText, 500);

    const handleFarm = (farm) => {
        // console.log({ farm });
    };

    const handleRegisterNewFarm = () => {
        let formikConfig = getInputs('newFarmAdmin');

        const updatedInputs = formikConfig.inputs.map((input) => {
            if (input.name === 'farmer') {
                const options = userList.map(({ _id, name, surname, identification }) => ({
                    value: _id,
                    label: `${name} ${surname} - ${identification}`
                }));

                return { ...input, options };
            } else {
                return input;
            }
        });

        formikConfig = {
            ...formikConfig,
            inputs: updatedInputs
        };

        setInputsForm(formikConfig);
        setDialogTitle('Registrar nueva finca');
        setDialogFormLabel('Registrar finca');
        setKeyForm('register_farm');
        setOpenDialog(true);
    };

    const getAllUsers = async () => {
        await getUsers().then((response) => {
            const { status, data } = response;

            if (status === 200) {
                const { docs } = data;
                setUserList(docs);
            } else {
                // console.log('Ha ocurrido un error');
            }
        });
    };

    const handleEditFarm = (form) => {
        switch (keyForm) {
            case 'register_farm':
                createFarmAndAssignFarmer({ data: form, tokenValue }).then((response) => {
                    const { status, data } = response;
                    if (status === 200) {
                        setOpenDialog(false);
                        getAllFarms();
                        SweetAlert({ title: '¡Bien hecho!', text: data.message, icon: 'success' });
                    } else {
                        SweetAlert({
                            title: '¡Atención!',
                            text: data.message || 'Ha ocurrido un error al registrar la finca',
                            icon: 'error'
                        });
                    }
                });
                break;
            default:
                break;
        }
    };

    const handleFilterChange = (event) => {
        setFilterText(event.target.value);
    };

    const getFarmsSearchKey = (keyword = '') => {
        getFarms({ keyword, tokenValue }).then((response) => {
            const { status, data } = response;

            if (status === 200) {
                const { docs } = data;
                let docsTwo = addFullNameToFarms({ farms: docs });
                setFarmList(docsTwo);
            } else {
                setFarmList([]);
            }
        });
    };

    const getAllFarms = () => {
        getFarms({ tokenValue }).then((response) => {
            const { status, data } = response;

            if (status === 200) {
                const { docs } = data;
                let docsTwo = addFullNameToFarms({ farms: docs });
                setFarmList(docsTwo);
            } else {
                setFarmList([]);
            }
        });
    };

    const addFullNameToFarms = ({ farms, farmerKey = 'farmer' }) => {
        return farms.map((farm) => {
            const farmer = farm[farmerKey];
            const fullName = `${farmer.name} ${farmer.surname}`;
            return { ...farm, farmerName: fullName };
        });
    };

    useEffect(() => {
        if (debouncedSearchUser) {
            //get farms with search key
            getFarmsSearchKey(debouncedSearchUser);
        } else {
            //get farms without search key
            getAllFarms();
        }
    }, [debouncedSearchUser]);

    useEffect(() => {
        getAllUsers();
    }, []);

    return (
        <div id="farms-page-admin" className="container-appbar">
            <div className="container-card">
                <div className="m-2">
                    <h1 className="text-lg font-semibold">Administrador, </h1>
                    <h1>
                        Acá puedes registrar fincas, actualizar información o asignar algún usuario
                        que elijas!
                    </h1>
                </div>
                <div id="farm-page-admin-actions" className="m-2">
                    <button className="mt-2 blue-button" onClick={() => handleRegisterNewFarm()}>
                        <AgricultureRoundedIcon className="mr-2" />
                        Registrar finca
                    </button>
                </div>
                {(farmList && farmList.length) > 0 ? (
                    <>
                        <div className="flex justify-center general-font m-3">
                            <TextField
                                className="text-field-search general-font"
                                label="Buscar finca"
                                value={filterText}
                                onChange={handleFilterChange}
                                InputProps={{
                                    startAdornment: (
                                        <InputAdornment position="start">
                                            <Search />
                                        </InputAdornment>
                                    )
                                }}
                                helperText="Buscar finca por nombre o administrador"
                            />
                        </div>

                        <TableComponent id="farms-page-admin-table" data={farmList} columns={columns} onFarmView={handleFarm} />
                    </>
                ) : farmList?.length === 0 ? (
                    <div id="farms-page-admin-message">
                        <h1 className="text-lg text-center font-bold m-3">
                            No hay fincas 🌾!
                        </h1>
                    </div>
                ) : (
                    <LoadingTextComponent />
                )}
            </div>

            <DialogComponent
                inputsForm={inputsForm}
                openDialog={openDialog}
                dialogTitle={dialogTitle}
                onCloseDialog={() => setOpenDialog(false)}
                labelForm={dialogFormLabel}
                onSubmit={handleEditFarm}
            />
        </div>
    );
};
