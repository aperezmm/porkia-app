import React, { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

//hooks
import { useUser } from '@/hooks/useUser';
//materialui
import AgricultureRoundedIcon from '@mui/icons-material/AgricultureRounded';
import GroupRoundedIcon from '@mui/icons-material/GroupRounded';
import PolylineRoundedIcon from '@mui/icons-material/PolylineRounded';

export const OverviewPageAdmin = () => {
    const { tokenValue, isAdmin, isTokenExpired, userInformation } = useUser();

    const navigate = useNavigate();

    return (
        <div id="overview-page" className="container-appbar">
            <div className="container-card">
                <div className="m-3">
                    <h1 className="text-lg font-semibold">
                        Hola, {userInformation.name + ' ' + userInformation.surname}!{' '}
                    </h1>
                </div>
                <div className="md:flex justify-around m-3 ">
                    <div className="sm:w-1/2 mx-auto md:w-1/3 rounded-md p-2 bg-gray-200 shadow-sm md:mx-2 my-2">
                        <div className="flex justify-center">
                            <button
                                className="blue-button mx-auto"
                                onClick={() => navigate('/main/admin/users')}
                            >
                                <GroupRoundedIcon className="mr-2" />
                                Administrar usuarios
                            </button>
                        </div>
                        <div className="text-center mt-2">
                            <h1>
                                Módulo para administrar todos los usuarios, registrar, editar o
                                cambiar contraseña.
                            </h1>
                        </div>
                    </div>

                    <div className="sm:w-1/2 mx-auto md:w-1/3 rounded-md p-2 bg-gray-200 shadow-sm md:mx-2 my-2">
                        <div className="flex justify-center">
                            <button
                                className="blue-button"
                                onClick={() => navigate('/main/admin/farms')}
                            >
                                <AgricultureRoundedIcon className="mr-2" />
                                Administrar fincas
                            </button>
                        </div>
                        <div className="text-center mt-2">
                            <h1>
                                Módulo para administrar las fincas, donde se puede registrar nueva finca a un administrador, siempre y cuando este registrado.
                            </h1>
                        </div>
                    </div>

                    <div className="sm:w-1/2 mx-auto md:w-1/3 rounded-md p-2 bg-gray-200 shadow-sm md:mx-2 my-2">
                        <div className="flex justify-center">
                            <button
                                className="blue-button"
                                onClick={() => navigate('/main/raw-materials')}
                            >
                                <PolylineRoundedIcon />
                                Administrar materias primas
                            </button>
                        </div>
                        <div className="text-center mt-2">
                            <h1>
                                Módulo para administrar las materias primas, donde se pueden ver todas las materias ingresadas, buscar y agregar nueva materia prima.
                            </h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};
