import React, { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

//Components
import { FarmsComponent } from '@/components/Farms/FarmsComponent';
//hooks
import { useUser } from '@/hooks/useUser';

export const OverviewPage = () => {
    const { tokenValue, isAdmin, isTokenExpired, userInformation } = useUser();

    const navigate = useNavigate();

    const handleFarmRegister = () => {
        // navigate('/main/user/farms');
    };

    useEffect(() => {
        if(!tokenValue || isTokenExpired){
            navigate('/login');
        }
    },[])

    return (
        <div id="overview-page" className="container-appbar">
            {/* FARMS COMPONENT */}
            <div className="px-2 py-6 container-card">
                <div className="m-2">
                    <h1 className="text-lg font-semibold">Hola, {userInformation.name + '!'}</h1>
                    <h1>Aquí tienes tus fincas!</h1>
                </div>
                <FarmsComponent />
            </div>

            {/* <h1>Prueba organizar etapas</h1>
            <MyComponent/> */}
        </div>
    );
};
