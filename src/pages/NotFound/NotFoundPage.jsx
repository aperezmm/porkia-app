import React from 'react';
import { useNavigate } from 'react-router-dom';
import { Box, Typography } from '@mui/material';
import { SentimentVeryDissatisfied } from '@mui/icons-material';

const styles = {
    container: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        minHeight: '100vh'
    },
    icon: {
        fontSize: '10rem',
        color: '#95c11f'
    },
    text: {
        // color: '#fff',
        fontSize: '1.5rem',
        fontWeight: 'bold',
        textAlign: 'center',
        marginTop: '2rem'
    }
};

export const NotFoundPage = () => {

    const navigate = useNavigate();

    return (
        <Box sx={styles.container}>
            
            <SentimentVeryDissatisfied sx={styles.icon} />
            <Typography sx={styles.text} variant="h1" component="h1">
                Página no encontrada
            </Typography>
            <button className="blue-button mt-2" onClick={() => navigate('/')}>Inicio</button>
        </Box>
    );
};
