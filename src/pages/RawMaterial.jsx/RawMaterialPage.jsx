
import { RawMaterialComponent } from "@/components/RawMaterial/RawMaterialComponent"

export const RawMaterialPage = () => {
    return (
        <div className="container-appbar ">
            <div className="container-card" id="raw-material-page">
                <RawMaterialComponent/>
            </div>
        </div>
    )
}