import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';

import { FormikDynamic } from '@/components/FormikDynamic';
import { getInputs } from '@/utils/getInputs';
import { SignInService } from '@/services/Auth/AuthManagement';
import { Layout } from '@/components/Layout';

import { SESSION_STORAGE_KEYS } from '@/constants/sessionStorageKeys';
import { SweetAlert } from '@/components/Alerts/SweetAlert';
//HOOKS
import { useUser } from '@/hooks/useUser';
//COMPONENTS
import { LoadingDotsComponent } from '@/components/LoadingDotsComponent';

const LoginPage = () => {
    const navigate = useNavigate();
    const { tokenValue, isTokenExpired } = useUser();

    const [isFetchingLoading, setIsFetchingLoading] = useState(false);

    const onSubmitLogin = (values) => {
        setIsFetchingLoading(true);
        SignInService({ data: values }).then((response) => {
            const { status, data } = response;
            setIsFetchingLoading(false);
            if (status === 200) {
                const { token } = data;
                //SET THE TOKEN IN SESSION STORAGE
                sessionStorage.setItem(SESSION_STORAGE_KEYS.ACCESS_TOKEN, token);
                navigate('/main');
            } else {
                const errorMessage = data.message || 'Error';
                SweetAlert({ title: '¡Atención!', text: errorMessage, icon: 'warning' });
            }
        });
    };

    useEffect(() => {
        if (tokenValue) {
            navigate('/main');
        }
    }, []);

    return (
        <div id="login-page" className="container-appbar">
            <Layout>
                <div className="container-login">
                    <div className="flex justify-center">
                        <h1 className="font-semibold">¡Bienvenido!</h1>
                    </div>
                    <div className="md:w-2/3 mx-auto">
                        <FormikDynamic
                            {...getInputs('login')}
                            onSubmit={onSubmitLogin}
                            labelButton="Iniciar sesión"
                            disabledButton={isFetchingLoading}
                        />
                    </div>
                    <div className="mx-auto mt-10">
                        <button
                            className="green-button"
                            onClick={() => navigate('/forget-password')}
                        >
                            {' '}
                            Olvidé mi contraseña
                        </button>
                    </div>
                </div>
            </Layout>
        </div>
    );
};

export default LoginPage;
