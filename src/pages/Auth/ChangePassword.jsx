import { useEffect, useState } from 'react';
//formik
import { FormikDynamic } from '@/components/FormikDynamic';
import { ChangeUserPassword } from '@/services/Auth/AuthManagement';

//hooks
import { useUser } from '@/hooks/useUser';
import { getInputs } from '@/utils/getInputs';
import { SweetAlert } from '@/components/Alerts/SweetAlert';
import { SweetAlertConfirm } from '@/components/Alerts/SweetAlertConfirm';
import { useNavigate } from 'react-router-dom';

export const ChangePasswordPage = () => {
    const { tokenValue, isAdmin, isTokenExpired, userInformation } = useUser();

    //forms formik states
    const [inputsForm, setInputsForm] = useState(null);

    const navigate = useNavigate();

    const inputsUser = () => {
        let formikConfig = getInputs('updateUserPassword');

        const updatedInputs = formikConfig.inputs.map((input) => {
            if (input.name === 'password') {
                return { ...input, show: true };
            } else {
                return { ...input };
            }
        });

        formikConfig = {
            ...formikConfig,
            inputs: updatedInputs
        };

        setInputsForm(formikConfig);
    };

    const checkPassword = (password) => {
        const regex = /^(?=.*[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?])(?=.*[A-Z]).{8,}$/;
        return regex.test(password);
    };

    const updatePassword = (data, { resetForm }) => {
        ChangeUserPassword({ data, tokenValue }).then((response) => {
            const { status, data } = response;
            if (status === 200) {
                SweetAlertConfirm({
                    title: '¡Bien hecho!',
                    text: data.message,
                    confirmButtonText: 'Ir al dashboard',
                    cancelButtonText: 'Cerrar',
                    icon: 'success'
                }).then((result) => {
                    if (result.isConfirmed) {
                        navigate('/main');
                    } else {
                        resetForm();
                    }
                });
            } else {
                SweetAlert({
                    title: '¡Atención!',
                    text: data.message || 'Ha ocurrido al actualizar la finca',
                    icon: 'error'
                });
            }
        });
    };

    const handleChangePassword = (form, { resetForm }) => {

        if (!checkPassword(form.confirmedPassword)) {
            SweetAlertConfirm({
                title: '¡Atención contraseña!',
                text: 'Se sugiere una contraseña que contenga mínimo un carácter especial, una letra mayúscula y mínimo de 8 caracteres de longitud. ¿Desea ignorar y actualizar?',
                confirmButtonText: 'Actualizar contraseña',
                cancelButtonText: 'Cancelar',
                icon: 'warning'
            }).then((result) => {
                if (result.isConfirmed) {
                    updatePassword(form, { resetForm });
                }
            });
        } else {
            //Continue with the update
            updatePassword(form, { resetForm });
        }
    };

    useEffect(() => {
        //configure the user inputs to show confirmed password
        inputsUser();
    }, []);

    return (
        <div id="forgot-password" className="container-appbar max-w-4xl mx-auto">
            <div className="container-card">
                <div className="m-2 text-center">
                    <h1 className="text-lg font-semibold">Señor usuario, </h1>
                    <h1>Ingresa y confirma tu nueva contraseña!</h1>
                </div>

                <div>
                    {inputsForm && (
                        <FormikDynamic
                            {...inputsForm}
                            onSubmit={handleChangePassword}
                            labelButton="Cambiar contraseña"
                        />
                    )}
                </div>
            </div>
        </div>
    );
};
