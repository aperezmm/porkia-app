
import { useEffect, useState } from "react";
import { useNavigate } from 'react-router-dom';
//formik
import { FormikDynamic } from "@/components/FormikDynamic";
import { getInputs } from "@/utils/getInputs";
import { ForgotUserPassword, GetAuthByLogInKey } from "@/services/Auth/AuthManagement";
import { SweetAlertConfirm } from "@/components/Alerts/SweetAlertConfirm";
import { SweetAlert } from "@/components/Alerts/SweetAlert";

export const ForgotPasswordPage = () => {

    const navigate = useNavigate();

    const [inputsForm, setInputsForm] = useState(getInputs('forgotPasswordLogInKey'));
    const [ isUserValidate, setIsUserValidate ] = useState(false);
    const [ userInformation, setUserInfomation ] = useState(null);

    const handleFindUser = (data) => {
        GetAuthByLogInKey({ data }).then((response) => {
            const { status, data } = response;

            if(status === 200){
                const { response } = data;
                setUserInfomation(response);
                setIsUserValidate(true);
                setInputsForm(getInputs('fotgotPassword'));
            } else {
                setIsUserValidate(false);
                SweetAlert({ title: '¡Atención!', text: data.message || "Ha ocurrido un error al consultar el usuario", icon: 'error' });
            }
        })
    }

    const checkPassword = (password) => {
        const regex = /^(?=.*[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?])(?=.*[A-Z]).{8,}$/;
        return regex.test(password);
    };

    const updateForgotUserPassword = ({ data }) => {
        //add the userId to update
        let newData = {
            ...data,
            userId: userInformation._id
        }
        //service to update
        ForgotUserPassword({ data: newData }).then((response) => {
            const { status, data } = response;
            if(status === 200){
                SweetAlertConfirm({
                    title: '¡Contraseña actualizada correctamente!',
                    text: data.message,
                    confirmButtonText: 'Iniciar sesión',
                    cancelButtonText: 'Cerrar',
                    icon: 'success'
                }).then((result) => {
                    if (result.isConfirmed) {
                        navigate('/login');
                    } else {
                        navigate('/login');
                    }
                });
            } else {
                SweetAlert({ title: '¡Atención!', text: data.message || "Ha ocurrido al actualizar la finca", icon: 'error' });
            }
        })
    }

    const handleChangeUserPassword = (data) => {

        if(!checkPassword(data.confirmedPassword)){
            SweetAlertConfirm({
                title: '¡Atención contraseña!',
                text: 'Se sugiere una contraseña que contenga mínimo un carácter especial, una letra mayúscula y mínimo de 8 caracteres de longitud. ¿Desea ignorar y actualizar?',
                confirmButtonText: 'Actualizar contraseña',
                cancelButtonText: 'Cancelar',
                icon: 'warning'
            }).then((result) => {
                if (result.isConfirmed) {
                    updateForgotUserPassword({ data });
                }
            });
        } else {
            updateForgotUserPassword({ data });
        } 

        
    }

    return (

        <div className="container-appbar">

            <div className="container-login">
                <div className="m-2 text-center">
                    <h1 className="text-lg font-semibold">Señor usuario, </h1>
                    {isUserValidate
                        ? <h1> Ahora ingresa y confirma tu nueva contraseña! </h1>
                        : <h1> Ingresa tu usuario o identificación! </h1>
                    }                    
                </div>
                <div className="md:w-2/3 mx-auto">
                    {isUserValidate 
                        ?
                            <FormikDynamic
                                {...inputsForm}
                                onSubmit={handleChangeUserPassword}
                                labelButton="Restaurar contraseña"
                            />


                        :
                            <FormikDynamic
                                {...inputsForm}
                                onSubmit={handleFindUser}
                                labelButton="Validar usuario"
                            />
                    }
                    
                </div>

                <div className="mx-auto m-3">
                    <button className="green-button" onClick={() => navigate('/login')}>He recordado mi contraseña</button>
                </div>
            </div>

        </div>
    )
}