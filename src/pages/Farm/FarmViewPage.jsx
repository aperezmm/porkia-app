import React, { useEffect, useState, createContext } from 'react';
import { useParams } from 'react-router-dom';
//hooks
import { useUser } from '@/hooks/useUser';
import { addDietByFarmId, getFarmById } from '@/services/Farm/FarmManagement';
//services
import { getInputs } from '@/utils/getInputs';
import { DialogComponent } from '@/components/Farms/DialogComponent';
import { getBreeds } from '@/services/User/UserManagement';
import { registerPig } from '@/services/Pig/PigManagement';
import { SweetAlert } from '@/components/Alerts/SweetAlert';
import { getDietsByFarmId } from '@/services/Farm/FarmManagement';
import { addStageByFarmId, addBarnyardByFarmId } from '@/services/Farm/FarmManagement';
//components
import { GeneralActionsComponent } from '@/components/Farms/GeneralActionsComponent';
import { TableFilterFarmsComponent } from '@/components/Farms/ListFilterFarmsComponent';
//material-ui
import ArrowDropDownRoundedIcon from '@mui/icons-material/ArrowDropDownRounded';
import ArrowDropUpRoundedIcon from '@mui/icons-material/ArrowDropUpRounded';
import AddRoundedIcon from '@mui/icons-material/AddRounded';
import { LoadingSpinner } from '@/components/LoadingSpinnerComponent';
import { DialogComponentFormulateDiet } from '@/components/Diet/DietDialogComponent';

//Context for some fields
export const FarmViewPageContext = createContext();

export const FarmViewPage = ({ children }) => {
    const { farmId } = useParams();
    const { tokenValue, isAdmin, isTokenExpired, userInformation } = useUser();

    const [farmInformation, setFarmInformation] = useState();

    const [farmDietsPendingFormulate, setFarmDietsPendingFormulate] = useState([]);
    const [farmDietsFormulated, setFarmDietsFormulated] = useState([]);
    //new batch and new pig
    const [openNewRegister, setNewRegister] = useState(false);
    const [openNewDietRegister, setNewDietRegister] = useState(false);

    const [loading, setLoading] = useState(true);
    //disbled formik button
    const [isFetchingLoading, setIsFetchingLoading] = useState(false);

    const [breeds, setBreeds] = useState([]);
    //forms formik states
    const [inputsForm, setInputsForm] = useState();
    //dialog states
    const [openDialog, setOpenDialog] = useState(false);
    const [openDialogFormulateDiet, setOpenDialogFormulateDiet] = useState(false);
    const [dialogTitle, setDialogTitle] = useState('');
    const [dialogFormLabel, setDialogFormLabel] = useState('');
    const [keyForm, setKeyForm] = useState('');

    const handleFarmAddStage = () => {
        //calculate the next stage orden
        const nextOrderStage = farmInformation.stages.length + 1;
        //Get the inputs to stage
        let formikConfig = getInputs('farmAddStage');

        formikConfig = {
            ...formikConfig,
            initialValues: {
                ...formikConfig.initialValues,
                order: nextOrderStage
            }
        };

        setInputsForm(formikConfig);
        setDialogTitle('Nueva etapa');
        setDialogFormLabel('Agregar etapa');
        //To know what api rest service use
        setKeyForm('new_stage');
        setOpenDialog(true);
    };

    const handleFarmAddBarnyard = () => {
        setInputsForm(getInputs('farmAddBarnyard'));
        setDialogTitle('Nuevo corral');
        setDialogFormLabel('Agregar corral');
        setKeyForm('new_barnyard');
        setOpenDialog(true);
    };

    const handleNewDietRegister = () => {
        setNewDietRegister(false);

        let formikConfig = getInputs('registerDietForm');

        setInputsForm(formikConfig);
        setDialogTitle('Registro de nueva dieta');
        setDialogFormLabel('Registrar dieta');
        setKeyForm('new_diet');
        setOpenDialog(true);
    };

    const handleNewRegisterPig = () => {
        setNewRegister(false);
        //newPigBatchForm
        let formikConfig = getInputs('newPigFarm');

        const updatedInputs = formikConfig.inputs.map((input) => {
            if (input.name === 'breed') {
                //Set the options
                const options = breeds.map(({ _id, name }) => ({
                    value: _id,
                    label: name
                }));

                return { ...input, options };
            } else {
                return input;
            }
        });

        formikConfig = {
            ...formikConfig,
            inputs: updatedInputs
        };

        setInputsForm(formikConfig);
        setDialogTitle('Registro de nuevo cerdo');
        setDialogFormLabel('Registrar cerdo');
        setKeyForm('new_pig');
        setOpenDialog(true);
    };

    const handleNewRegisterBatch = () => {
        setNewRegister(false);

        let formikConfig = getInputs('newPigBatchForm');

        const updatedInputs = formikConfig.inputs.map((input) => {
            if (input.name === 'breed') {
                //Set the options
                const options = breeds.map(({ _id, name }) => ({
                    value: _id,
                    label: name
                }));

                return { ...input, options };
            } else {
                return input;
            }
        });

        formikConfig = {
            ...formikConfig,
            inputs: updatedInputs
        };

        setInputsForm(formikConfig);
        setDialogTitle('Registro de cerdos por lote');
        setDialogFormLabel('Registrar lote');
        setKeyForm('new_batch');
        setOpenDialog(true);
    };

    const onSubmitForm = (data) => {
        switch (keyForm) {
            case 'new_diet':
                setIsFetchingLoading(true);
                addDietByFarmId({ data, farmId, tokenValue }).then((response) => {
                    const { status, data } = response;
                    setIsFetchingLoading(false);
                    if (status === 200) {
                        const { response, message } = data;
                        setOpenDialog(false);
                        getFarmDiets(farmId);
                        SweetAlert({
                            title: '¡Bien hecho!',
                            text: message || '¡Dieta registrada exitosamente!',
                            icon: 'success'
                        });
                    } else {
                        SweetAlert({
                            title: '¡Ha ocurrido un error!',
                            text: data.message || '¡Ha ocurrido un error al registrar la dieta!',
                            icon: 'error'
                        });
                    }
                });
                break;
            case 'new_pig':
                let newData = {
                    ...data,
                    farm_id: farmId,
                    birth_weight: parseInt(data.birth_weight)
                };
                setIsFetchingLoading(true);
                registerPig({ data: newData, tokenValue }).then((response) => {
                    const { status, data } = response;
                    setIsFetchingLoading(false);
                    if (status === 200) {
                        const { response, message } = data;
                        setOpenDialog(false);
                        //if there is new breed update the list
                        if(response?.isNewBreed){
                            getAllBreeds();
                        }
                        SweetAlert({
                            title: '¡Bien hecho!',
                            text: message || '¡Cerdo registrado exitosamente!',
                            icon: 'success'
                        });
                        //update the current information
                        getFarmInformation();
                    } else {
                        SweetAlert({
                            title: '¡Ha ocurrido un error!',
                            text: data.message || '¡Ha ocurrido un error al registrar el cerdo!',
                            icon: 'error'
                        });
                    }
                });
                break;

            case 'new_batch':
                setIsFetchingLoading(true);
                let newBatchData = {
                    ...data,
                    farm_id: farmId,
                    birth_weight: parseInt(data.birth_weight),
                    quantity_males: parseInt(data.quantity_males),
                    male_percentage:
                        (parseInt(data.quantity_males) / parseInt(data.initial_batch_size)) * 100,
                    initial_batch_size: parseInt(data.initial_batch_size),
                    final_batch_size: parseInt(data.final_batch_size)
                };

                registerPig({ data: newBatchData, tokenValue }).then((response) => {
                    const { status, data } = response;
                    setIsFetchingLoading(false);
                    if (status === 200) {
                        const { response, message } = data;
                        setOpenDialog(false);
                        //if there is new breed update the list
                        if(response?.isNewBreed){
                            getAllBreeds();
                        }
                        SweetAlert({
                            title: '¡Bien hecho!',
                            text: message || '¡Lote de cerdo registrados exitosamente!',
                            icon: 'success'
                        });
                        //update the current information
                        getFarmInformation();
                    } else {
                        SweetAlert({
                            title: '¡Ha ocurrido un error!',
                            text:
                                data.message ||
                                '¡Ha ocurrido un error al registrar el lote de cerdos!',
                            icon: 'error'
                        });
                    }
                });

                break;

            case 'new_stage':
                setIsFetchingLoading(true);
                //call api service
                addStageByFarmId({ data, farmId, tokenValue }).then((response) => {
                    const { status, data } = response;
                    setIsFetchingLoading(false);
                    if (status === 200) {
                        setOpenDialog(false);
                        SweetAlert({
                            title: 'Bien hecho',
                            text: data.message || 'Etapa registrada correctamente',
                            icon: 'success'
                        });
                        //update the current information
                        getFarmInformation();
                    } else {
                        if (status === 409) {
                            SweetAlert({
                                title: 'Atención',
                                text: data.message || 'Etapa ya registrada',
                                icon: 'warning'
                            });
                        } else {
                            SweetAlert({
                                title: 'Atención',
                                text: data.message || 'Error al registrar la etapa',
                                icon: 'warning'
                            });
                        }
                    }
                });
                break;

            case 'new_barnyard':
                setIsFetchingLoading(true);
                addBarnyardByFarmId({ data, farmId, tokenValue }).then((response) => {
                    const { status, data } = response;
                    setIsFetchingLoading(false);
                    if (status === 200) {
                        setOpenDialog(false);
                        SweetAlert({
                            title: 'Bien hecho',
                            text: data.message || 'Corral registrado correctamente',
                            icon: 'success'
                        });
                        //update the current information
                        getFarmInformation();
                    } else {
                        if (status === 409) {
                            SweetAlert({
                                title: 'Atención',
                                text: data.message || 'Corral ya registrado',
                                icon: 'warning'
                            });
                        }
                    }
                });
                break;

            default:
                break;
        }
    };
    //Dropdown buttons pigs
    const handleOpenNewRegister = () => {
        setNewRegister(!openNewRegister);
    };
    //Dropdown buttons diets
    const handleOpenNewDietRegister = () => {
        setNewDietRegister(!openNewDietRegister);
    };

    const getAllBreeds = () => {
        getBreeds({ tokenValue }).then((response) => {
            const { status, data } = response;

            if (status === 200) {
                const { response } = data;
                setBreeds(response);
            }
        });
    };

    const getFarmDiets = (farmId) => {
        getDietsByFarmId({ farmId, tokenValue }).then((response) => {
            const { status, data } = response;

            if (status === 200) {
                const { response } = data;
                //Filter diets pending to formulate
                const dietsPendingFormulated = response.filter((diet) => {
                    return diet.isFormulated === false;
                });

                setFarmDietsPendingFormulate(dietsPendingFormulated);

                const dietsFormulated = response.filter((diet) => {
                    return diet.isFormulated === true;
                })
                
                setFarmDietsFormulated(dietsFormulated);
                // setFarmDiets(response);
            }
        });
    };

    const getFarmInformation = () => {
        getFarmById({ farmId, tokenValue }).then((response) => {
            const { status, data } = response;

            if (status === 200) {
                const { response } = data;
                setLoading(false);
                setFarmInformation(response);
            } else {
                setLoading(false);
            }
        });
    };

    const onCloseDialog = () => {
        setOpenDialog(false);
    };

    const onCloseDialogFormulateDiet = () => {
        setOpenDialogFormulateDiet(false);
    };

    const handleFormulateDiet = () => {
        setNewDietRegister(false);
        setOpenDialogFormulateDiet(true);
    };

    useEffect(() => {
        getFarmInformation();
        getAllBreeds();
        getFarmDiets(farmId);
    }, []);

    return (
        <FarmViewPageContext.Provider value={{ farmDietsPendingFormulate, farmDietsFormulated, farmInformation }}>
            <div className="container-appbar">
                <div className="flex justify-between">
                    <div className="m-2" id="info-farm">
                        {farmInformation && (
                            <>
                                <h1 className="title-farms general-font">NOMBRE FINCA: {farmInformation.name}</h1>
                                <h2>
                                    UBICACIÓN: {farmInformation.country}, {farmInformation.department},{' '}
                                    {farmInformation.city}
                                </h2>
                            </>
                        )}
                    </div>
                    <div className="flex items-end my-2">
                        {/* <h2 className="text-lg text-center">Nuevos registros</h2> */}
                        <div className="flex">
                            {/* Breeds, newness and cause of death */}
                            <GeneralActionsComponent updateBreeds={getAllBreeds} />

                            <DialogComponent
                                inputsForm={inputsForm}
                                openDialog={openDialog}
                                dialogTitle={dialogTitle}
                                onCloseDialog={onCloseDialog}
                                labelForm={dialogFormLabel}
                                onSubmit={onSubmitForm}
                                disabledOnSubmit={isFetchingLoading}
                            />
                        </div>
                    </div>
                </div>
                {/* Filter and table */}
                {loading ? (
                    <div className="flex-column justify-center text-center m-3 ">
                        {/* <LoadingSpinner/> */}
                        <h2 className="text-lg font-bold">Cargando información...</h2>
                    </div>
                ) : (
                    <div className="container-card">
                        <div className="m-3">
                            <h1 className="text-lg">Información relevante de la finca</h1>
                            <div className="">
                                <h2 className="text-lg">
                                    Cantidad de cerdos: {farmInformation?.pigs.length || 0}
                                </h2>
                            </div>
                            <div className="flex justify-between mb-2 mt-2" id="actions-farm">
                                <div className="flex ">
                                    {/* new pig or batch */}
                                    <div className="dropdown">
                                        <button
                                            className="blue-button m-1"
                                            onClick={() => handleOpenNewRegister()}
                                        >
                                            Nuevo cerdo/lote
                                            {openNewRegister ? (
                                                <ArrowDropUpRoundedIcon />
                                            ) : (
                                                <ArrowDropDownRoundedIcon />
                                            )}
                                        </button>
                                        {openNewRegister ? (
                                            <ul className="menu">
                                                <li className="menu-item">
                                                    <button
                                                        className="green-button"
                                                        onClick={() => handleNewRegisterPig()}
                                                    >
                                                        Nuevo cerdo
                                                    </button>
                                                </li>
                                                <li className="menu-item">
                                                    <button
                                                        className="green-button"
                                                        onClick={() => handleNewRegisterBatch()}
                                                    >
                                                        Nuevo lote
                                                    </button>
                                                </li>
                                            </ul>
                                        ) : null}
                                    </div>
                                    {/* diets */}
                                    <div className="dropdown">
                                        <button
                                            className="blue-button m-1"
                                            onClick={() => handleOpenNewDietRegister()}
                                        >
                                            Configuración dietas
                                            {openNewDietRegister ? (
                                                <ArrowDropUpRoundedIcon />
                                            ) : (
                                                <ArrowDropDownRoundedIcon />
                                            )}
                                        </button>
                                        {openNewDietRegister ? (
                                            <ul className="menu">
                                                <li className="menu-item">
                                                    <button
                                                        className="green-button"
                                                        onClick={() => handleNewDietRegister()}
                                                    >
                                                        Nueva dieta
                                                    </button>
                                                </li>
                                                <li className="menu-item">
                                                    <button
                                                        className="green-button"
                                                        onClick={() => handleFormulateDiet()}
                                                    >
                                                        Configurar dieta
                                                    </button>
                                                </li>
                                            </ul>
                                        ) : null}
                                    </div>
                                </div>

                                <div className="flex">
                                    {/* stage */}
                                    <div className="m-1">
                                        <button
                                            className="green-button"
                                            onClick={() => handleFarmAddStage()}
                                        >
                                            <AddRoundedIcon />
                                            Nueva etapa
                                        </button>
                                    </div>
                                    {/* barynard */}
                                    <div className="m-1">
                                        <button
                                            className="green-button"
                                            onClick={() => handleFarmAddBarnyard()}
                                        >
                                            <AddRoundedIcon />
                                            Nuevo corral
                                        </button>
                                    </div>
                                </div>
                            </div>

                            {/* Component to show table with farms data and search key */}
                            {farmInformation && (
                                <TableFilterFarmsComponent farmInformation={farmInformation} tokenValue={tokenValue}/>
                            )}
                        </div>
                    </div>
                )}

                <DialogComponentFormulateDiet
                    openDialog={openDialogFormulateDiet}
                    onCloseDialog={onCloseDialogFormulateDiet}
                    onFarmDiets={getFarmDiets}
                    farmDietsPendingFormulate={farmDietsPendingFormulate}
                    farm={farmInformation}
                />
            </div>
        </FarmViewPageContext.Provider>
    );
};
