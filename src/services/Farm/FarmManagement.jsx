import ApiRestService from "@/services/ApiRestService";

export async function createFarmAndAssignFarmer({data, tokenValue}) {
    const { name, department, country, city, farmer } = data;
    return await ApiRestService().post('/farm/register-farm',
        { name, department, country, city, farmer },
        { headers: { 'bialtec-token': tokenValue } }
    )
        .then((response) => {
            const { status, data } = response;
            return { status, data };
        })
        .catch((error) => {
            try {
                const { data, status } = error.response;
                return { data, status };
            } catch (error) {
                return { data : {}};
            }
        })
}

export async function getFarms({keyword="", tokenValue}) {
    return await ApiRestService().get(`/farm/all?searchKey=${keyword}`, null, {
        headers: { "bialtec-token": tokenValue }
    })
        .then((response) => {
            // console.log({response});
            const { status, data } = response;
            return { status, data};
        })
        .catch((error) => {
            try {
                const { data, status } = error.response;
                return { data, status };
            } catch (error) {
                return { data : {}};
            }
        })
}

export async function getFarmById({farmId, tokenValue}) {
    return await ApiRestService().get(`/farm/${farmId}`, null, {
        headers: { "bialtec-token": tokenValue }
    })
        .then((response) => {
            // console.log({response});
            const { status, data } = response;
            return { status, data};
        })
        .catch((error) => {
            try {
                const { data, status } = error.response;
                return { data, status };
            } catch (error) {
                return { data : {}};
            }
        })
}

export async function getAllFarmsByFarmer({keyword = "", farmerId, tokenValue}) {
    return await ApiRestService().get(`/farm/all/${farmerId}?searchKey=${keyword}`, null, {
        headers: { "bialtec-token": tokenValue }
    })
        .then((response) => {
            const { status, data } = response;
            return { status, data};
        })
        .catch((error) => {
            try {
                const { data, status } = error.response;
                return { data, status };
            } catch (error) {
                return { data : {}};
            }
        })
}

export async function addStageByFarmId({data, farmId, tokenValue}) {
    const {name, order} = data;
    return await ApiRestService().put(`/farm/add-stage/${farmId}`, 
        { name, order }, 
        { headers: { 'bialtec-token': tokenValue } }
    )
        .then((response) => {
            const { status, data } = response;
            return { status, data };
        })
        .catch((error) => {
            try {
                const { data, status } = error.response;
                return { data, status };
            } catch (error) {
                return { data : {}};
            }
        })
}

export async function addBarnyardByFarmId({data, farmId, tokenValue}) {
    const { name, area } = data;
    return await ApiRestService().put(`/farm/add-barnyard/${farmId}`, 
        { name, area }, 
        { headers: { 'bialtec-token': tokenValue } }
    )
        .then((response) => {
            const { status, data } = response;
            return { status, data };
        })
        .catch((error) => {
            try {
                const { data, status } = error.response;
                return { data, status };
            } catch (error) {
                return { data : {}};
            }
        })
}

export async function addDietByFarmId({data, farmId, tokenValue}){
    const { name, price } = data;
    return await ApiRestService().post(`/farm/add-diet/${farmId}`,
        { name, price },
        { headers: { 'bialtec-token': tokenValue } }
    )
        .then((response) => {
            const { status, data } = response;
            return { status, data };
        })
        .catch((error) => {
            try {
                const { data, status } = error.response;
                return { data, status };
            } catch (error) {
                return { data : {}};
            }
        })
}

export async function getDietsByFarmId({farmId, tokenValue}) {
    return await ApiRestService().get(`/farm/get-diets/${farmId}`, null, {
        headers: { "bialtec-token": tokenValue }
    })
        .then((response) => {
            const { status, data } = response;
            return { status, data};
        })
        .catch((error) => {
            try {
                const { data, status } = error.response;
                return { data, status };
            } catch (error) {
                return { data : {}};
            }
        })
}