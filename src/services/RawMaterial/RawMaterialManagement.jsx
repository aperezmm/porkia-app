import ApiRestService from "@/services/ApiRestService";

export async function getAllRawMaterials({keyword = "",  tokenValue }) {
    return await ApiRestService().get(`/raw-material/all?searchKey=${keyword}`, null, {
        headers: { "bialtec-token": tokenValue }
    })
        .then((response) => {
            const { status, data } = response;
            return { status, data};
        })
        .catch((error) => {
            try {
                const { data, status } = error.response;
                return { data, status };
            } catch (error) {
                return { data : {}};
            }
        })
}
//Get all raw materials, without pagination.
export async function getRawMaterialList({ tokenValue }) {
    return await ApiRestService().get(`/raw-material/all/list`, null, {
        headers: { "bialtec-token": tokenValue }
    })
        .then((response) => {
            const { status, data } = response;
            return { status, data};
        })
        .catch((error) => {
            try {
                const { data, status } = error.response;
                return { data, status };
            } catch (error) {
                return { data : {}};
            }
        })
}

export async function addRawMaterial({data, tokenValue}) {
    const { name } = data;
    return await ApiRestService().post('/raw-material/add',
        { name },
        { headers: {'bialtec-token': tokenValue } }
    )
        .then((response) => {
            const { status, data } = response;
            return { status, data};
        })
        .catch((error) => {
            try {
                const { data, status } = error.response;
                return { data, status };
            } catch (error) {
                return { data : {}};
            }
        })
}