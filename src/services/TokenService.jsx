import jwtDecode from "jwt-decode";
import { SESSION_STORAGE_KEYS } from "@/constants/sessionStorageKeys";

let instance = null;

class TokenService {

    getToken() {
        return sessionStorage.getItem(SESSION_STORAGE_KEYS.ACCESS_TOKEN);
    }

    getTokenData() {
        const token = sessionStorage.getItem(SESSION_STORAGE_KEYS.ACCESS_TOKEN);
        if(token){
            return jwtDecode(token);
        }
    }

    saveToken(token) {
        sessionStorage.setItem(SESSION_STORAGE_KEYS.ACCESS_TOKEN, token);
    }

    getUserRole() {
        const token = this.getToken();
        if (token) {
            const tokenData = jwtDecode(this.getToken());
            const rol = tokenData.roles[0];
            return rol;
        } else {
            return null;
        }
    }

}

/**@returns {TokenService} */
const Service = () => {
    if (!instance) {

        instance = new TokenService();

    }
    return instance;
}

export default Service;