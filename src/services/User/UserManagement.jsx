import ApiRestService from "@/services/ApiRestService";

export async function getUsers({ keyword = "", tokenValue} = {}){
    return await ApiRestService().get(`/user/users/all?searchKey=${keyword}`, null, {
        headers: { "bialtec-token": tokenValue }
    })
        .then((response) => {
            const { status, data } = response;
            return { status, data};
        })
        .catch((error) => {
            try {
                const { data, status } = error.response;
                return { data, status };
            } catch (error) {
                return { data : {}};
            }
        })
}

export async function getUserById({ tokenValue, userId }){
    return await ApiRestService().get(`/user/${userId}`, null, {
        headers: { "bialtec-token" : tokenValue }
    })
        .then((response) => {
            const { status, data } = response;
            return { status, data };
        })
        .catch((err) => {
            try {
                const { data, status } = err.response;
                return { data, status };
            } catch (error) {
                return { data : {}};
            }
        })
}

export async function updateUserPassword({ tokenValue, data }){
    const { userId, confirmedPassword, userIdentification } = data;
    return await ApiRestService().put(`/admin/change-user-password/${userId}`,
        {
            userIdentification,
            newPassword: confirmedPassword
        }, {
        headers: { "bialtec-token": tokenValue }
    })
        .then((response) => {
            const { status, data } = response;
            return { status, data };
        })
        .catch((err) => {
            try {
                const { data, status } = err.response;
                return { data, status };
            } catch (error) {
                return { data : {}};
            }
        })
}

export async function updateUserInformation({ tokenValue, data, userId }){
    const { email, username, name, surname, cellphone, about_me, country } = data;
    return await ApiRestService().put(`/admin/update-user-information/${userId}`,
        {
            email, username, name, surname, cellphone, about_me, country
        }, {
            headers: { "bialtec-token": tokenValue }
    })
        .then((response) => {
            const { status, data } = response;
            return { status, data };
        })
        .catch((err) => {
            try {
                const { data, status } = err.response;
                return { data, status };
            } catch (error) {
                return { data : {}};
            }
        })
}

export async function addCauseDeath({data, tokenValue}) {
    const { name } = data;
    return await ApiRestService().post(`/cause-death/add-cause-death`, 
        { name }, 
        { headers: { 'bialtec-token': tokenValue } }
    )
        .then((response) => {
            const { status, data } = response;
            return { status, data };
        })
        .catch((error) => {
            try {
                const { data, status } = error.response;
                return { data, status };
            } catch (error) {
                return { data : {}};
            }
        })
}

export async function getCauseDeaths({tokenValue}){
    return await ApiRestService().get('/cause-death/all/farmer', null, {
        headers: { "bialtec-token": tokenValue }
    })
        .then((response) => {
            const { status, data } = response;
            return { status, data };
        })
        .catch((error) => {
            try {
                const { data, status } = error.response;
                return { data, status };
            } catch (error) {
                return { data : {}};
            }
        })
}

export async function addNewness({data, tokenValue}) {
    const { name } = data;
    return await ApiRestService().post(`/newness/add-newness`, 
        { name }, 
        { headers: { 'bialtec-token': tokenValue } }
    )
        .then((response) => {
            const { status, data } = response;
            return { status, data };
        })
        .catch((error) => {
            try {
                const { data, status } = error.response;
                return { data, status };
            } catch (error) {
                return { data : {}};
            }
        })
}

export async function getNewness({tokenValue}){
    return await ApiRestService().get('/newness/all/farmer', null, {
        headers: { "bialtec-token": tokenValue }
    })
        .then((response) => {
            const { status, data } = response;
            return { status, data };
        })
        .catch((error) => {
            try {
                const { data, status } = error.response;
                return { data, status };
            } catch (error) {
                return { data : {}};
            }
        })
}

export async function addBreed({data, tokenValue}) {
    const { name } = data;
    return await ApiRestService().post(`/breed/add-breed`, 
        { name }, 
        { headers: { 'bialtec-token': tokenValue } }
    )
        .then((response) => {
            const { status, data } = response;
            return { status, data };
        })
        .catch((error) => {
            try {
                const { data, status } = error.response;
                return { data, status };
            } catch (error) {
                return { data : {}};
            }
        })
}

export async function getBreeds({tokenValue}){
    return await ApiRestService().get('/breed/all/farmer', null, {
        headers: { "bialtec-token": tokenValue }
    })
        .then((response) => {
            const { status, data } = response;
            return { status, data };
        })
        .catch((error) => {
            try {
                const { data, status } = error.response;
                return { data, status };
            } catch (error) {
                return { data : {}};
            }
        })
}
