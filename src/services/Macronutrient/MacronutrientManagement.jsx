import ApiRestService from "@/services/ApiRestService";

export async function createMacronutrient({data, tokenValue}) {
    const { name } = data;
    return await ApiRestService().post('/macronutrient/add-macronutrient',
        { name }, 
        { headers: { 'bialtec-token': tokenValue } }
    )
        .then((response) => {
            const { status, data } = response;
            return { status, data };
        })
        .catch((error) => {
            try {
                const { data, status } = error.response;
                return { data, status };
            } catch (error) {
                return { data : {}};
            }
        })
}

export async function getMacronutrients({tokenValue}) {
    return await ApiRestService().get('/macronutrient/all', null , {
        headers: { 'bialtec-token' : tokenValue }
    })
        .then((response) => {
            const { status, data } = response;
            return { status, data};
        })
        .catch((error) => {
            try {
                const { data, status } = error.response;
                return { data, status };
            } catch (error) {
                return { data : {}};
            }
        })
}