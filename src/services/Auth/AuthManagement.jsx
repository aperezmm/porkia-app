import ApiRestService from "@/services/ApiRestService";

export async function SignInService({data}){
    const { logInKey, password } = data;
    return await ApiRestService().post('/auth/sign-in', {
        logInKey,
        password
    })
        .then((response) => {
            const { status, data } = response;
            return {status, data};
        })
        .catch((err) => {
            try {
                const { data, status } = err.response;
                return { data, status };
            } catch (error) {
                return { data : {}};
            }
        })
}

export async function SignUpService({data, tokenValue}){
    // console.log({tokenValue});
    const { email, username, password, identification, name, surname, country, about_me, cellphone, roles, created_by } = data;

    return await ApiRestService().post('/auth/sign-up', {
        email,
        username,
        password,
        identification,
        name,
        surname,
        cellphone,
        country,
        about_me,
        roles,
        created_by
    }, {
        headers: { "bialtec-token": tokenValue}
    })
        .then((response) => {
            const { status, data } = response;
            return {status, data};
        })
        .catch((error) => {
            const { data, status } = error.response;

            return {status, data};
        })
}

export async function GetAuthByLogInKey({ data }) {
    const { logInKey } = data;
    return await ApiRestService().get(`/auth/find-auth?logInkey=${logInKey}`, 
        { logInKey },
        null    
    )
        .then((response) => {
            const { status, data } = response;
            return { status, data };
        })
        .catch((error) => {
            try {
                const { data, status } = error.response;
                return { data, status };
            } catch (error) {
                return { data : {}};
            }
        })
}

export async function ForgotUserPassword({ data }) {
    const { userId, newPassword, confirmedPassword } = data;
    return await ApiRestService().put('/auth/forgot-password',
        { userId, newPassword, confirmedPassword } ,
        null
    )
        .then((response) => {
            const { status, data } = response;
            return { status, data };
        })
        .catch((error) => {
            try {
                const { data, status } = error.response;
                return { data, status };
            } catch (error) {
                return { data : {}};
            }
        })
}

export async function ChangeUserPassword({data, tokenValue}) {
    return await ApiRestService().put('/auth/change-password',
        { ...data },
        { headers: { 'bialtec-token': tokenValue } }
    )
        .then((response) => {
            const { status, data } = response;
            return { status, data };
        })
        .catch((error) => {
            try {
                const { data, status } = error.response;
                return { data, status };
            } catch (error) {
                return { data : {}};
            }
        })
}