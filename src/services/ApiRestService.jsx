import axios from 'axios';

let instance = null;

class ApiRestService {

    constructor(host, port, path) {
        this.host = host;
        this.port = port;
        this.path = path;
        this.config = {};
    }

    getCancelToken() {
        return axios.CancelToken.source();
    }

    /**
     * @returns {Promise}
     * @param {*} path 
     * @param {*} params 
     * @param {*} options 
     */
    get(path, params = {}, options = {}, source = 'unknow') {
        // console.log('GET SOURCE----> ', source, { path });
        const url = `${this.host}:${this.port}/${this.path}`;
        return axios.get(`${url}${path}`, {
            ...options,
            params: { ...params }
        });
    }

    /**
     * @returns {Promise}
     * @param {*} path 
     * @param {*} data 
     * @param {*} config 
     */
    post(path, data = {}, config = {}, source = 'unknow') {
        // console.log('POST SOURCE----> ', source, { path });
        const url = `${this.host}:${this.port}/${this.path}`;
        return axios.post(`${url}${path}`, data, config)
    }

    /**
     * @returns {Promise}
     * @param {*} path 
     * @param {*} data 
     * @param {*} config 
     */
    put(path, data = {}, config = {}, source='unknow') {
        // console.log('PUT SOURCE----> ', source, { path });
        const url = `${this.host}:${this.port}/${this.path}`;
        return axios.put(`${url}${path}`, data, config)
    }

}

/**@returns {ApiRestService} */
const Service = () => {
    if (!instance) {
        const host = import.meta.env.VITE_REACT_APP_API_HOST;
        const port = import.meta.env.VITE_REACT_APP_API_PORT;
        const path = import.meta.env.VITE_REACT_APP_API_PATH;

        instance = new ApiRestService(host, port, path);

    }
    return instance;
}

export default Service;
