import ApiRestService from "@/services/ApiRestService";

export async function registerPig({data, tokenValue}) {
    return await ApiRestService().post('/pig/add-pig', 
        { ...data },
        { headers: { 'bialtec-token': tokenValue } }
    )
        .then((response) => {
            const { status, data } = response;
            return { status, data };
        })
        .catch((error) => {
            try {
                const { data, status } = error.response;
                return { data, status };
            } catch (error) {
                return { data : {}};
            }
        })
}

export async function getStagesPig({ pigCode, is_batch_record, tokenValue}) {
    //build the route
    let route = is_batch_record
        ? `/stage/get-all-stages/${pigCode}?is_batch_record=true` 
        : `/stage/get-all-stages/${pigCode}`

    console.log({is_batch_record, route});

    return await ApiRestService().get(route, null, {
        headers: { "bialtec-token": tokenValue }
    })
        .then((response) => {
            // console.log({response});
            const { status, data } = response;
            return { status, data};
        })
        .catch((error) => {
            try {
                const { data, status } = error.response;
                return { data, status };
            } catch (error) {
                return { data : {}};
            }
        })
}