import ApiRestService from "@/services/ApiRestService";


export async function formulateDietByDietId({data, dietId, tokenValue}) {
    return await ApiRestService().put(`/diet/formulate-diet/${dietId}`,
        { ...data },
        { headers: { 'bialtec-token': tokenValue } }
    )
        .then((response) => {
            const { status, data } = response;
            return { status, data };
        })
        .catch((error) => {
            try {
                const { data, status } = error.response;
                return { data, status };
            } catch (error) {
                return { data : {}};
            }
        })
}