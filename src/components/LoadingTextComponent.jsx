export const LoadingTextComponent = () => {
    return (
        <div>
            <h1 className="text-lg text-center font-bold m-3">Cargando ... 🌾!</h1>
        </div>
    );
};
