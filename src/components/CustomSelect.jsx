import { ErrorMessage, useField } from "formik";

export const CustomSelect = ({ label, ...props }) => {
    const [field] = useField(props)

    return (
        <>

            <div>
                <label htmlFor={props.name || props.id}>{label}</label>
                <select {...field} {...props} >
                    <option value="">Seleccionar...</option>
                    {
                        props.options.map(({ label, value }) => (
                            <option
                                value={value}
                                key={value}
                            >{label}</option>
                        ))
                    }
                </select>
            </div>
            <ErrorMessage name={props.name} component="span" className="error" />
        </>
    )
}