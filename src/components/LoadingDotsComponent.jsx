import React from 'react';

export function LoadingDotsComponent() {
  return (
    <h1 className="loading-dots-wrapper">
      <span className="loading-dots loading-dot-1">.</span>
      <span className="loading-dots loading-dot-2">.</span>
      <span className="loading-dots loading-dot-3">.</span>
    </h1>
  );
}
