import React from "react";

export function LoadingSpinner() {
  return (
    <div className="lazy-loading-wrapper">
      <div className="loading-circle" />
    </div>
  );
}