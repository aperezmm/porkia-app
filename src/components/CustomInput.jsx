import { ErrorMessage, useField } from "formik"

export const CustomTextInput = (props) => {

    const { show, ...restProps } = props;

    const [field] = useField(props)

    return show ? (
        <>
            <input {...field} {...restProps} value={field.value || ''} />
            <ErrorMessage name={restProps.name} component="span" className="error" />
        </>
    ) : null;
}