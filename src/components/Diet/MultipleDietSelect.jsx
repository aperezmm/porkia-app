import React, { useState, useEffect } from 'react';
import { FormControl, InputLabel, MenuItem, Select } from '@mui/material';

export const MultipleDietSelectComponent = ({ label, options, disabled, onChange, ...props }) => {

    const [selectedValues, setSelectedValues] = useState([]);

    const handleChange = (event) => {
        const { value } = event.target;
        setSelectedValues(value);
        onChange(value);
    };

    return (
        <FormControl fullWidth>
            <InputLabel>{label}</InputLabel>
            <Select
                disabled={disabled}
                multiple
                value={selectedValues}
                onChange={handleChange}
                label={label}
                {...props}
            >
                {options.map((option) => (
                    <MenuItem key={option._id} value={option._id}>
                        {option.name}
                    </MenuItem>
                ))}
            </Select>
        </FormControl>
    );
};
