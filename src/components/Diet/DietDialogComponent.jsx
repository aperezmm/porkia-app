import React, { useState, useEffect, useRef, useContext } from 'react';
import { Close } from '@mui/icons-material';
import { Dialog, DialogTitle, DialogContent, IconButton } from '@mui/material';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import HelpRoundedIcon from '@mui/icons-material/HelpRounded';
import Tooltip from '@mui/material/Tooltip';
//components
import { DietSelectComponent } from './DietSelect';
import { MultipleDietSelectComponent } from './MultipleDietSelect';
//services
import { getAllRawMaterials, getRawMaterialList } from '@/services/RawMaterial/RawMaterialManagement';
import { getDietsByFarmId } from '@/services/Farm/FarmManagement';
//Hooks
import { useUser } from '@/hooks/useUser';
import { DietPercentageTableComponent, DietMacronutrientTableComponent } from './DietTablesComponent';
import { getMacronutrients } from '@/services/Macronutrient/MacronutrientManagement';
import { formulateDietByDietId } from '@/services/Diet/DietManagement';
import { SweetAlert } from '@/components/Alerts/SweetAlert';
import { SweetAlertConfirm } from '@/components/Alerts/SweetAlertConfirm';
import { FarmViewPageContext } from '@/pages/Farm/FarmViewPage';

export const DialogComponentFormulateDiet = ({
    dialogTitleFormulateDiet = 'Configuración de dietas',
    openDialog,
    onCloseDialog,
    onFarmDiets,
    farm
}) => {
    //Ref of the dialog
    const dialogContentRef = useRef(null);
    const { tokenValue, isAdmin, isTokenExpired, userInformation } = useUser();

    const { farmDietsPendingFormulate } = useContext(FarmViewPageContext);

    const [hasScroll, setHasScroll] = useState(false);

    //diet selected is an object
    const [selectedDiet, setSelectedDiet] = useState('');
    //List of diets
    const [ dietList, setDietList ] = useState(farmDietsPendingFormulate);
    //List of raw materials
    const [rawMaterialList, setRawMaterialList] = useState([]);
    //List of macronutrient
    const [macronutrientList, setMacronutrientList] = useState([]);
    //List of raw materials selected
    const [rawMaterialsSelected, setRawMaterialsSelected] = useState([]);
    //List of raw materials selected with percetange
    const [rawMaterialsSelectedWithPercentage, setRawMaterialsSelectedWithPercentage] = useState([]);

    //buttons states
    const [isFormulateDiet, setIsFormulateDiet] = useState(false);
    const [isEditFormulateDiet, setIsEditFormulateDiet] = useState(false);

    //steps of diet formulation
    const [isStepOne, setIsStepOne] = useState(true);
    const [isStepTwo, setIsStepTwo] = useState(false);

    /**
     * Every the raw material selected changes this functions set the
     * variable with the necessary information
     * @param rawMaterialsSelected array of objects
     */
    const handleSelectedRawMaterials = (rawMaterialsSelected) => {
        const filteredRawMaterials = rawMaterialList.filter((rawMaterial) =>
            rawMaterialsSelected.includes(rawMaterial._id)
        );
        //assign default percentage
        const addPerceFilteredRawMaterials = filteredRawMaterials.map((rawMaterial) => {
            return { ...rawMaterial, percentage: 0 }
        })

        setRawMaterialsSelected(addPerceFilteredRawMaterials);
    };

    /**
     * Every handle on diet change, set the selected diet to save the state
     * @param  diet 
     */
    const handleDietChange = (diet) => {
        setSelectedDiet(diet);
    };

    /**
     * This function resets various states and closes a dialog box.
     */
    const handleCloseDialog = () => {
        if(selectedDiet){
            SweetAlertConfirm({
                title: '¡Atención!',
                text: "¡Esta seguro que desea salir sin realizar el registro?",
                confirmButtonText: 'Sí',
                cancelButtonText: 'Cancelar',
                icon: 'warning'
            }).then((result) => {
                if (result.isConfirmed) {
                    setSelectedDiet(null);
                    setIsStepOne(true);
                    setIsStepTwo(false);
                    setRawMaterialsSelected([]);
                    setIsFormulateDiet(false);

                    onCloseDialog();
                }
            });
        } else {
            onCloseDialog();
        }
        
    };

    /**
     * This function handles the selection of raw materials and their percentages, closes the raw
     * material percentage section, and opens the macronutrient section.
     */
    const handleStepTwo = (rawMaterialTablePercetanges) => {
        setRawMaterialsSelectedWithPercentage(rawMaterialTablePercetanges);
        setIsStepOne(false);
        setIsStepTwo(true);
    };

    /**
     * This function sets the selected raw materials and macronutrient list and updates the step
     * status.
     */
    const handleStepOne = (macronutrientTableData) => {
        setRawMaterialsSelected(rawMaterialsSelectedWithPercentage);
        setMacronutrientList(macronutrientTableData);
        setIsStepOne(true);
        setIsStepTwo(false);
    }

    /**
     * This function handles the confirmation and formulation of a diet with given macronutrient data.
     */
    const handleFormulateDietDialog = (macronutrientsTableData) => {

        SweetAlertConfirm({
            title: '¡Confirmación!',
            text: `Nombre de la dieta a formular: ${selectedDiet.name}. ¿Desea guardar la formulación?`,
            confirmButtonText: 'Sí',
            cancelButtonText: 'Cancelar',
            icon: 'warning'
        }).then((result) => {
            if (result.isConfirmed) {
                const updateSelectedDiet = {
                    ...selectedDiet,
                    macronutrients: macronutrientsTableData,
                    rawMaterials: rawMaterialsSelectedWithPercentage
                }
        
                const { _id } = updateSelectedDiet;
        
                formulateDietByDietId({data: updateSelectedDiet, dietId: _id, tokenValue}).then((response) => {
                    const { status, data } = response;

                    if(status === 200){
                        SweetAlertConfirm({
                            title: '¡Bien hecho!',
                            text: "Dieta formulada exitosamente",
                            confirmButtonText: 'Salir',
                            cancelButtonText: 'Formular otra dieta',
                            icon: 'success'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                handleCloseDialog();
                            } else {
                                //clean states
                                setSelectedDiet('');
                                setIsStepOne(true);
                                setIsStepTwo(false);
                                //get just diets to formulate
                                onFarmDiets(farm._id);
                                setRawMaterialsSelected([]);
                                //set the macronutrient list
                                const cleanMacronutrientData = macronutrientList.map((macronutrient) => {
                                    return { ...macronutrient, unit:"", lot:0 }
                                })
                                setMacronutrientList(cleanMacronutrientData);
                            }
                        });
                    } else {
                        SweetAlert({
                            title: '¡Atención!',
                            text: data.message || 'Ha ocurrido al formular la dieta',
                            icon: 'error'
                        })
                    }
                });
            }
        });
    };

    const handleFormulateDiet = () => {
        setIsFormulateDiet(true);
        if (isEditFormulateDiet) {
            setIsEditFormulateDiet(false);
        }
    };

    const handleEditFormulateDiet = () => {
        setIsEditFormulateDiet(true);
        if (isFormulateDiet) {
            setIsFormulateDiet(false);
        }
    };

    const handleScrollToBottom = () => {
        const { current: content } = dialogContentRef;
        content.scrollTo(0, content.scrollHeight);
    };

    useEffect(() => {
        setDietList(farmDietsPendingFormulate);
    },[farmDietsPendingFormulate])

    useEffect(() => {
        getRawMaterialList({ tokenValue }).then((response) => {
            const { status, data } = response;

            if (status === 200) {
                const { response } = data;
                setRawMaterialList(response);
            }
        });

        getMacronutrients({ tokenValue }).then((response) => {
            const { status, data } = response;

            if (status === 200) {
                const { response } = data;
                const addLotUnitMacronutrientData = response.map((macronutrient) => {
                    return { ...macronutrient, unit:"", lot:0 }
                })
                setMacronutrientList(addLotUnitMacronutrientData);
            }
        });
        
    }, []);

    useEffect(() => {
        if(openDialog){
            const cleanMacronutrientData = macronutrientList.map((macronutrient) => {
                return { ...macronutrient, unit:"", lot:0 }
            })

            setMacronutrientList(cleanMacronutrientData);
        }
    },[openDialog])

    useEffect(() => {
        const { current } = dialogContentRef;
        if (current) {
            const { clientHeight, scrollHeight } = current;
            setHasScroll(scrollHeight > clientHeight);
        }
    }, [rawMaterialsSelected, isStepTwo, isStepOne]);

    return (
        <Dialog open={openDialog} fullScreen className="p-10">
            <DialogTitle className="flex justify-between">
                <div className="mx-auto my-auto">
                    <h1 className="font-semibold">{dialogTitleFormulateDiet}</h1>
                </div>
                <IconButton aria-label="close-icon" onClick={handleCloseDialog}>
                    <Close />
                </IconButton>
            </DialogTitle>
            <DialogContent className="custom-scrollbar" ref={dialogContentRef}>
                <div>
                    <div className="flex justify-around">
                        <div className="flex">
                            <button className="blue-button" onClick={handleFormulateDiet}>
                                Formular dieta
                            </button>
                            <div className="my-auto opacity-40 cursor-help">
                                
                                <Tooltip title="Agregar porcentajes a las materias primas y macronutrientes">
                                    <HelpRoundedIcon fontSize="small"/>
                                </Tooltip>
                            </div>
                        </div>
                        {/* <div className="flex">
                            <button className="blue-button" onClick={handleEditFormulateDiet}>
                                Editar formulación de dietas
                            </button>
                            <div className="my-auto opacity-40 cursor-help">
                                
                                <Tooltip title="Editar información de una dieta formulada">
                                    <HelpRoundedIcon fontSize="small"/>
                                </Tooltip>
                            </div>
                        </div> */}
                    </div>
                    {isFormulateDiet && (
                        <>
                            <div className="m-2">
                                {/* <h1 className="font-semibold text-center">Formulación de nueva dieta</h1> */}
                                <h1 className="py-2 text-sm">
                                    Por favor, busque y seleccione la dieta para realizar la
                                    formulación
                                </h1>
                                <DietSelectComponent
                                    label="Dieta a formular"
                                    options={dietList}
                                    onChange={handleDietChange}
                                    MenuProps={{
                                        PaperProps: {
                                            style: {
                                                maxHeight: '150px'
                                            }
                                        }
                                    }}
                                />
                            </div>
                            {selectedDiet && (
                                <>
                                    <div className="m-2">
                                        <h1 className="py-2 text-sm">
                                            Ahora deberas seleccionar las materias primas para
                                            asignar porcentajes
                                        </h1>
                                        <MultipleDietSelectComponent
                                            label="Materias primas"
                                            options={rawMaterialList}
                                            onChange={handleSelectedRawMaterials}
                                            disabled={isStepTwo ? true : false}
                                            MenuProps={{
                                                PaperProps: {
                                                    style: {
                                                        maxHeight: '150px'
                                                    }
                                                }
                                            }}
                                        />
                                    </div>
                                    <div>
                                        {rawMaterialsSelected.length > 0 && (
                                            <>
                                                <div className="p-2 mx-auto md:w-1/2">
                                                    {/* show this table to assign the percentage */}
                                                    {isStepOne && (
                                                        <div>
                                                            <h1 className="text-sm mb-2 text-center">Ingresar los porcentajes de las materias primas</h1>
                                                            <DietPercentageTableComponent
                                                                rawMaterials={
                                                                    rawMaterialsSelected
                                                                }
                                                                handleStepTwo={handleStepTwo}
                                                            />
                                                        </div>
                                                    )}
                                                    {/* show this table to assign value a lot of macronutrients */}
                                                    {isStepTwo && (
                                                        <div>
                                                            <DietMacronutrientTableComponent
                                                                onHandleBack={handleStepOne}
                                                                macronutrientData={
                                                                    macronutrientList
                                                                }
                                                                handleFormulateDiet={
                                                                    handleFormulateDietDialog
                                                                }
                                                            />
                                                        </div>
                                                    )}
                                                </div>
                                            </>
                                        )}
                                    </div>
                                </>
                            )}
                        </>
                    )}
                </div>
                { hasScroll &&
                    <div className="scroll-arrow" onClick={handleScrollToBottom}>
                        <ArrowDropDownIcon />
                    </div>
                }                
            </DialogContent>
        </Dialog>
    );
};
