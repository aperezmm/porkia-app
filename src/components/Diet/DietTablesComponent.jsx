import React, { useState, useEffect } from 'react';
//components
import { SweetAlert } from '@/components/Alerts/SweetAlert';
import TableContainer from '@mui/material/TableContainer';
import Paper from '@mui/material/Paper';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import TextField from '@mui/material/TextField';
import InputAdornment from '@mui/material/InputAdornment';

export function DietPercentageTableComponent({rawMaterials, handleStepTwo }) {
    const [rawMaterialTableData, setRawMaterialTableData] = useState(rawMaterials);

    const handlePercentageChange = (event, row) => {
        let { value } = event.target;
        value = parseInt(value, 10);
        //Set the data to preview data of raw materials
        setRawMaterialTableData((prevData) => {
            const newData = prevData.map((prevRow) => {
                if (prevRow._id === row._id) {
                    if (value > 100) {
                        value = 100;
                    }
                    return { ...prevRow, percentage: value || 0 };
                }

                return prevRow;
            });
            return newData;
        });
    };

    const handleNextStep = () => {
        let totalPercentage = 0;
        totalPercentage = rawMaterialTableData.reduce((accumulator, current) => {
            return accumulator + parseInt(current.percentage);
        }, 0);

        if (totalPercentage === 100) {
            handleStepTwo(rawMaterialTableData);
        } else {
            SweetAlert({
                title: '¡Atención!',
                text:
                    'Para formular la dieta, la suma de todos los porcentajes debe ser 100. ' +
                    'Actualmente suman ' +
                    (totalPercentage || 0) +
                    '.',
                icon: 'warning'
            });
        }
    };

    useEffect(() => {
        const updatedRawMaterialTableData = rawMaterials.map((item) => {
            const matchingItem = rawMaterialTableData.find((data) => data._id === item._id);
            if (matchingItem) {
                return {
                    ...item,
                    percentage: matchingItem.percentage
                };
            }
            return item;
        });

        setRawMaterialTableData(updatedRawMaterialTableData);
    }, [rawMaterials]);

    return (
        <>
            <div className="flex justify-end my-2">
                <button className="green-button" onClick={handleNextStep}>
                    Pasar a los macronutrientes
                </button>
            </div>
            <TableContainer component={Paper} style={{ backgroundColor: '#ccc' }}>
                <Table aria-label="percentage table diet">
                    <TableHead>
                        <TableRow>
                            <TableCell>
                                <h1 className="text-center font-semibold">Materia prima</h1>
                            </TableCell>
                            <TableCell>
                                <h1 className="text-center font-semibold">Porcentaje</h1>
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {rawMaterialTableData.map((rawMaterial, index) => (
                            <TableRow key={rawMaterial.name}>
                                <TableCell component="th" scope="rawMaterial" className="custom-table-padding">
                                    <h1 className="text-center">{rawMaterial.name}</h1>
                                </TableCell>
                                <TableCell className="custom-table-padding">
                                    <div className="flex justify-center">
                                        <TextField
                                        className="custom-text-field-height"
                                            type="number"
                                            value={rawMaterial.percentage}
                                            onChange={(event) =>
                                                handlePercentageChange(event, rawMaterial)
                                            }
                                            InputProps={{
                                                inputProps: {
                                                    max: 100
                                                },
                                                endAdornment: (
                                                    <InputAdornment position="end">
                                                        %
                                                    </InputAdornment>
                                                )
                                            }}
                                            helperText=""
                                        />
                                    </div>
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
            
        </>
    );
}

export function DietMacronutrientTableComponent({ macronutrientData, handleFormulateDiet, onHandleBack }) {
    const [tableMacronutrientData, setTableMacronutrientData] = useState(macronutrientData);

    /**
     * Function to listen the changes about the lot
     */
    const handleLotChange = (event, row) => {
        let { value } = event.target;
        value = parseInt(value, 10);
        //Set the data to preview data of raw materials
        setTableMacronutrientData((prevData) => {
            const newData = prevData.map((prevRow) => {
                if (prevRow._id === row._id) {
                    return { ...prevRow, lot: value || 0 };
                }

                return prevRow;
            });

            return newData;
        });
    };

    /**
     * Function to listen the changes about the unit
     */
    const handleUnitChange = (event, row) => {
        const { value } = event.target;
        //Set the data to preview data of raw materials
        setTableMacronutrientData((prevData) => {
            const newData = prevData.map((prevRow) => {
                if (prevRow._id === row._id) {
                    return { ...prevRow, unit: value };
                }

                return prevRow;
            });

            return newData;
        });
    };

    /**
     * This function allow validate the formulation to avoid send empty data, and show errors in the table
     * @params macronutrientData = Is the data of the table
     */
    const validateFormulateDiet = (macronutrientData) => {
        //build errors
        const verifyErrorFields = macronutrientData.map((macronutrient) => {
            if (!macronutrient.unit) {
                return { ...macronutrient, error: true };
            } else {
                return { ...macronutrient, error: false };
            }
        });
        setTableMacronutrientData(verifyErrorFields);
        //if there are some with error, don't save
        return verifyErrorFields.every((macronutrient) => !macronutrient.error);
    };

    /**
     * This function call other function to validate the data of table, if there aren't errors then should
     * continue with the save
     */
    const onClickFormulateDiet = () => {
        if (validateFormulateDiet(tableMacronutrientData)) {
            handleFormulateDiet(tableMacronutrientData);
        }
    };

    const onClickBackDiet = () => {
        onHandleBack(tableMacronutrientData);
    }

    return (
        <>
             <div className="flex justify-between my-2">
            <   button className="blue-button" onClick={onClickBackDiet}>
                    Atras
                </button>
                <button className="green-button" onClick={onClickFormulateDiet}>
                    Formular dieta
                </button>
            </div>
            <TableContainer component={Paper} style={{ backgroundColor: '#ccc' }}>
                <Table aria-label="macronutrient table diet">
                    <TableHead>
                        <TableRow>
                            <TableCell>
                                <h1 className="text-center font-semibold">Macronutrientes</h1>
                            </TableCell>
                            <TableCell>
                                <h1 className="text-center font-semibold">Unidad</h1>
                            </TableCell>
                            <TableCell>
                                <h1 className="text-center font-semibold">Cantidad</h1>
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {tableMacronutrientData.map((macronutrient, index) => (
                            <TableRow key={macronutrient.name}>
                                <TableCell component="th" scope="macronutrient" className="custom-table-padding">
                                    <h1 className="text-center">{macronutrient.name}</h1>
                                </TableCell>
                                <TableCell className="custom-table-padding">
                                    <div className="flex justify-center">
                                        <TextField
                                            className="custom-text-field-height"
                                            error={macronutrient?.error}
                                            type="text"
                                            value={macronutrient.unit}
                                            onChange={(event) =>
                                                handleUnitChange(event, macronutrient)
                                            }
                                            required
                                            helperText={
                                                macronutrient.error ? 'Campo requerido' : ''
                                            }
                                        />
                                    </div>
                                </TableCell>
                                <TableCell className="custom-table-padding">
                                    <div className="flex justify-center">
                                        <TextField
                                            className="custom-text-field-height"
                                            type="number"
                                            value={macronutrient.lot}
                                            onChange={(event) =>
                                                handleLotChange(event, macronutrient)
                                            }
                                            InputProps={{
                                                inputProps: {
                                                    max: 100,
                                                    min: 0
                                                }
                                            }}
                                            helperText=""
                                        />
                                    </div>
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
           
        </>
    );
}
