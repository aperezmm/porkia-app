import React, { useState } from 'react';
import { FormControl, InputLabel, MenuItem, Select } from '@mui/material';

export const DietSelectComponent = ({ label, options, onChange, ...props }) => {

    const [selectedValues, setSelectedValues] = useState([]);

    const handleChange = (event) => {
        const { value } = event.target;
        setSelectedValues(value);
        
        const selectedOption = options.find(option => option._id === value);
        onChange(selectedOption);
    };

    return (
        <FormControl fullWidth>
            <InputLabel>{label}</InputLabel>
            <Select
                value={selectedValues}
                onChange={handleChange}
                label={label}
                {...props}
            >
                {options.map((option) => (
                    <MenuItem key={option._id} value={option._id}>
                        {option.name}
                    </MenuItem>
                ))}
            </Select>
        </FormControl>
    );
};

