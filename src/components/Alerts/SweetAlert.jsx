import Swal from "sweetalert2";

export const SweetAlert = ({ title, text, icon }) => {
    
    Swal.fire({
        customClass: {
            container: 'swal-container'
        },
        title: title,
        text: text,
        icon: icon,
        confirmButtonColor: '#013284'
    });
};
