import Swal from "sweetalert2";

export const SweetAlertConfirm = ({title, text, confirmButtonText, cancelButtonText, icon}) => {
    return Swal.fire({
        customClass: {
            container: 'swal-container'
        },
        icon: icon,
        confirmButtonColor: '#013284',
        cancelButtonColor: '#95c11f',
        title: title,
        text: text,
        showCancelButton: true,
        confirmButtonText: confirmButtonText,
        cancelButtonText: cancelButtonText
    })
}