import React, { useEffect, useState } from 'react';
//Components
import { TableComponent } from '@/components/TableDynamic';
//material ui
import { TextField, InputAdornment } from '@mui/material';
import { Search } from '@mui/icons-material';
//hooks
import { useUser } from '@/hooks/useUser';
import { useDebounce } from '@/hooks/useDebounce';
import { getAllRawMaterials } from '@/services/RawMaterial/RawMaterialManagement';
import { SweetAlert } from '@/components/Alerts/SweetAlert';
import { LoadingTextComponent } from '../LoadingTextComponent';

const columns = [{ key: 'name', label: 'Nombre materia' }];

export const ListRawMaterialComponent = ({ updateRawMaterials, onUpdateStatusRawMaterial }) => {
    const { tokenValue, isAdmin, isTokenExpired, userInformation } = useUser();

    const [filterText, setFilterText] = useState('');
    const debouncedSearchFarm = useDebounce(filterText, 500);

    const [rawMaterialList, setRawMaterialList] = useState(null);

    const handleFilterChange = (event) => {
        setFilterText(event.target.value);
    };

    const onGetAllRawMaterials = (keyword) => {
        getAllRawMaterials({ keyword, tokenValue }).then((response) => {
            const { status, data } = response;

            if (status === 200) {
                const { docs } = data;
                setRawMaterialList(docs);
                onUpdateStatusRawMaterial();
            } else {
                setRawMaterialList([]);
                SweetAlert({
                    title: '¡Error!',
                    text:
                        data.message ||
                        'Lo sentimos, ha ocurrido un error inesperado en nuestro servidor. Por favor, intentalo de nuevo o contácta al equipo de soporte. Gracias por tu comprensión.',
                    icon: 'error'
                });
            }
        });
    };

    useEffect(() => {
        onGetAllRawMaterials();
    }, [updateRawMaterials]);

    useEffect(() => {
        if (debouncedSearchFarm) {
            onGetAllRawMaterials(debouncedSearchFarm);
        } else {
            onGetAllRawMaterials();
        }
    }, [debouncedSearchFarm]);

    return (
        <>
            {(rawMaterialList && rawMaterialList.length) > 0 ? (
                <>
                    <div className="flex justify-center general-font">
                <TextField
                    className="text-field-search general-font"
                    label="Buscar materia prima"
                    value={filterText}
                    onChange={handleFilterChange}
                    InputProps={{
                        startAdornment: (
                            <InputAdornment position="start">
                                <Search />
                            </InputAdornment>
                        )
                    }}
                    helperText="Puede buscar por nombre"
                />
            </div>

            <TableComponent data={rawMaterialList} columns={columns} showActions={false} />
                </>
            ) : rawMaterialList?.length === 0 ? (
                <div id="farms-page-message">
                        <h1 className="text-lg text-center font-bold m-3">
                            No hay materias 🌾!
                        </h1>
                    </div>
            ) : (
                <LoadingTextComponent />
            )
        
        }
            
            
        </>
    );
};
