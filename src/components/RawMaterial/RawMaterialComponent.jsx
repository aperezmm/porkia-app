import React, { useEffect, useState } from 'react';
//components
import { ListRawMaterialComponent } from "@/components/RawMaterial/ListRawMaterial"

//hooks
import { useUser } from '@/hooks/useUser';
import { DialogComponentRawMaterials } from '@/components/RawMaterial/DialogComponent';

//services
import { getInputs } from '@/utils/getInputs';
import { addRawMaterial } from '@/services/RawMaterial/RawMaterialManagement';
import { SweetAlert } from '@/components/Alerts/SweetAlert';
//materialui
import AddRoundedIcon from '@mui/icons-material/AddRounded';

export const RawMaterialComponent = (props) => {

    const { tokenValue, isAdmin, isTokenExpired, userInformation } = useUser();

    //forms formik states
    const [inputsForm, setInputsForm] = useState();
    //dialog states
    const [openDialog, setOpenDialog] = useState(false);
    const [dialogTitle, setDialogTitle] = useState('');
    const [dialogFormLabel, setDialogFormLabel] = useState('');

    const [ updateRawMaterials, setUpdateRawMaterials ] = useState(false);
    

    const onSubmitForm = (data) => {

        addRawMaterial({data, tokenValue}).then((response) => {
            const { status, data } = response;
            if (status === 200) {
                setOpenDialog(false);
                setUpdateRawMaterials(true);
                SweetAlert({
                    title: 'Bien hecho',
                    text: data.message || 'Materia registrada correctamente',
                    icon: 'success'
                });
            } else {
                if (status === 409) {
                    SweetAlert({
                        title: 'Atención',
                        text: data.message || 'La materia que intentas registrar ya se encuentra registrada',
                        icon: 'warning'
                    });
                }
            }
        })
    }

    const handleNewRegister = () => {

        let formikConfig = getInputs('newRawMaterial');

        setInputsForm(formikConfig);
        setDialogTitle('Registrar nueva materia prima');
        setDialogFormLabel('Registrar');
        setOpenDialog(true);
    };


    return (
        <>
            <div className='mx-4 my-2'>
                <div className="text-center">
                    <h1 className="font-semibold">Señor usuario,</h1>
                    <h1>Esta es la lista de materias primas disponibles, puedes agregar otras!</h1>
                    
                </div>
            </div>
            {/* New raw material */}
            <div className='mx-4 my-2'>
                <button
                    className="blue-button m-1"
                    onClick={() => handleNewRegister()}
                >   
                    <AddRoundedIcon/>
                    Agregar materia
                </button>
            </div>
            {/* Table with search key */}
            <div className="m-4">
                <ListRawMaterialComponent
                    updateRawMaterials={updateRawMaterials}
                    onUpdateStatusRawMaterial={() => setUpdateRawMaterials(false)}
                />
            </div>

            <DialogComponentRawMaterials
                inputsForm={inputsForm}
                openDialog={openDialog}
                dialogTitle={dialogTitle}
                onCloseDialog={() => setOpenDialog(false)}
                labelForm={dialogFormLabel}
                onSubmit={onSubmitForm}
            />

        </>
    )
}