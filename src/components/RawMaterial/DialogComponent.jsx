import { Close } from '@mui/icons-material';
import { Dialog, DialogTitle, DialogContent, IconButton } from '@mui/material';

//components
import { FormikDynamic } from '@/components/FormikDynamic';

export const DialogComponentRawMaterials = ({
    dialogTitle = 'Titulo',
    openDialog,
    onCloseDialog,
    onSubmit,
    inputsForm,
    labelForm = ''
}) => {
    return (
        <Dialog open={openDialog} id="dialog-farms" maxWidth="sm" fullWidth={true} >
            <DialogTitle className="flex justify-between">
                <div className="mx-auto my-auto">
                    <h1 className="font-semibold">{dialogTitle}</h1>
                </div>
                <IconButton aria-label="close-icon" onClick={onCloseDialog}>
                    <Close />
                </IconButton>
            </DialogTitle>
            <DialogContent className="custom-scrollbar">
                {/* <div>
                    prueba
                </div> */}
                <div>
                    {/* Forms */}
                    <FormikDynamic
                        {...inputsForm}
                        onSubmit={onSubmit}
                        labelButton={labelForm}
                    />
                </div>
            </DialogContent>
        </Dialog>
    );
};
