import { useState, useRef } from 'react';
import {
    List,
    ListItemButton,
    ListItemText,
    ListItemSecondaryAction,
    IconButton
} from '@mui/material';
import { ArrowUpward } from '@mui/icons-material';

export function MyComponent() {
    const [items, setItems] = useState(['Etapa 1', 'Etapa 2', 'Etapa 3', 'Etapa 4']);
    const [selectedIndex, setSelectedIndex] = useState(-1);
    const dragIndexRef = useRef(null);

    function handleSelect(index) {
        setSelectedIndex(index);
    }

    function handleMouseDown(index) {
        dragIndexRef.current = index;
    }

    function handleMouseUp(index) {
        if (dragIndexRef.current !== null && dragIndexRef.current !== index) {
            const newItems = [...items];
            const [removedItem] = newItems.splice(dragIndexRef.current, 1);
            newItems.splice(index, 0, removedItem);
            setItems(newItems);
            setSelectedIndex(index);
            dragIndexRef.current = null;
        }
    }

    function handleMouseMove(index) {
        if (dragIndexRef.current !== null && dragIndexRef.current !== index) {
            setSelectedIndex(index);
        }
    }

    return (
        <div className="mt-2">
            <hr></hr>
            <h1>Organizar etapas</h1>
            <List className="general-font">
                {items.map((item, index) => (
                    <ListItemButton
                        key={index}
                        selected={selectedIndex === index}
                        onMouseDown={() => handleMouseDown(index)}
                        onMouseUp={() => handleMouseUp(index)}
                        onMouseMove={() => handleMouseMove(index)}
                        sx={{
                            '&.Mui-selected': {
                                backgroundColor: '#ebebeb',
                                color: '#2C3639'
                            },
                            ...(selectedIndex === index ? { backgroundColor: 'pink' } : {})
                        }}
                    >
                        <ListItemText primary={item} />
                        <ListItemSecondaryAction>
                            <IconButton aria-label="Select" onClick={() => handleSelect(index)}>
                                <ArrowUpward />
                            </IconButton>
                        </ListItemSecondaryAction>
                    </ListItemButton>
                ))}
            </List>
        </div>
    );
}
