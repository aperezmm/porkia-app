import { ErrorMessage, useField } from 'formik';
import { MenuItem, TextField } from '@mui/material';

export const CustomSelect = ({ label, options, ...props}) => {

    const [field, meta, helpers] = useField(props);

    return (
        <>
            <TextField
                fullWidth
                {...field}
                {...props}
                label={label}
                select
                error={meta.touched && !!meta.error}
                helperText={meta.touched && meta.error}
            >
                {/* <MenuItem value="">
                    <em>Seleccionar...</em>
                </MenuItem> */}

                { options.map((option, index) => (
                    <MenuItem key={index} value={option.value}>
                        {option.label}
                    </MenuItem>
                ))}
            </TextField>
        </>
    )

}
