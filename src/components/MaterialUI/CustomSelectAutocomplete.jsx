import { ErrorMessage, useField } from 'formik';
import Autocomplete from '@mui/material/Autocomplete';
import TextField from '@mui/material/TextField';

export const CustomSelectAutocomplete = ({ label, options, ...props }) => {
    const [field, meta, helpers] = useField(props);

    // console.log(field.value);

    return (
        <>
            <Autocomplete
                {...field}
                {...props}
                freeSolo
                options={options.map((option) => option.label)}
                value={field.value}
                // onChange={(event, value) => (console.log({value}))}
                onInputChange={(event, value) => {
                    helpers.setValue(value);
                }}
                // getOptionSelected={(option, value) => option.value === value}
                getOptionLabel={(option) => (option ? option.label || option : '')}
                renderInput={(params) => (
                    <TextField
                        {...params}
                        label={label}
                        error={meta.touched && !!meta.error}
                        helperText={meta.touched && meta.error}
                    />
                )}
            />
        </>
    );
};
