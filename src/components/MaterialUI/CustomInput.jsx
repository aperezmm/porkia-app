import { useField } from "formik";
import { TextField } from '@mui/material';

export const CustomTextInput = (props) => {

    const { show, ...restProps } = props;

    const [field, meta] = useField(props);

    return show ? (
        <>
            <TextField
                fullWidth
                {...field}
                {...restProps}
                value={field.value || ''}
                error={meta.touched && !!meta.error}
                helperText={meta.touched && meta.error}
            />
        </>
    ) : null;
}