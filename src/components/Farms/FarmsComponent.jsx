import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

//hooks
import { useUser } from '@/hooks/useUser';
import { useDebounce } from '@/hooks/useDebounce';

//components
import { TableComponent } from '@/components/TableDynamic';
import { DialogComponent } from '@/components/Farms/DialogComponent';

//Services
import { createFarmAndAssignFarmer, getAllFarmsByFarmer } from '@/services/Farm/FarmManagement';

import { getInputs } from '@/utils/getInputs';
import { SweetAlert } from '@/components/Alerts/SweetAlert';

//material ui
import { TextField, InputAdornment } from '@mui/material';
import { Search } from '@mui/icons-material';
import { SweetAlertConfirm } from '@/components/Alerts/SweetAlertConfirm';
import AgricultureRoundedIcon from '@mui/icons-material/AgricultureRounded';

//Columns for table
const columns = [
    { key: 'name', label: 'NOMBRE' },
    { key: 'country', label: 'PAIS' },
    { key: 'department', label: 'DEPARTAMENTO' },
    { key: 'city', label: 'CIUDAD' }
];

export const FarmsComponent = (props) => {
    const navigate = useNavigate();
    const { tokenValue, isAdmin, isTokenExpired, userInformation } = useUser();
    const [farmsUserList, setFarmsUserList] = useState([]);
    const [loading, setLoading] = useState(true);
    const [emptyResult, setEmptyResult] = useState(false);
    //forms formik states
    const [inputsForm, setInputsForm] = useState({});
    const [filterText, setFilterText] = useState('');
    const debouncedSearchFarm = useDebounce(filterText, 500);
    //dialog states
    const [openDialog, setOpenDialog] = useState(false);
    const [dialogTitle, setDialogTitle] = useState('');
    const [dialogFormLabel, setDialogFormLabel] = useState('');
    const [keyForm, setKeyForm] = useState('');
    const [farmId, setFarmId] = useState(null);

    const handleFarmRegister = () => {
        setInputsForm(getInputs('newFarm'));
        setDialogTitle('Nueva finca');
        setDialogFormLabel('Registrar finca');
        setKeyForm('new_farm');
        setFarmId(null);
        openDialogContent();
    };

    const onSubmitForm = (data) => {
        //Depends of the the key to know what API we should use
        switch (keyForm) {
            case 'new_farm':
                const newData = { ...data, farmer: userInformation.id };
                createFarmAndAssignFarmer({ data: newData, tokenValue }).then((response) => {
                    const { status, data } = response;
                    if (status === 200) {
                        setOpenDialog(false);
                        getFarmsByFarmer();
                        SweetAlert({
                            title: '¡Bien hecho!',
                            text: data.message || 'Finca registrada exitosamente',
                            icon: 'success'
                        });
                    }
                });
                break;

            default:
                break;
        }
    };

    //Open the dialog
    const openDialogContent = () => {
        setOpenDialog(true);
    };

    const handleViewFarm = (farm) => {
        navigate(`/main/user/farms/${farm._id}`);
    };

    const getFarmsByFarmer = () => {
        getAllFarmsByFarmer({ farmerId: userInformation.id, tokenValue }).then((response) => {
            const { status, data } = response;

            if (status === 200) {
                const { docs } = data;
                if (docs.length === 0) {
                    setEmptyResult(true);
                } else {
                    setEmptyResult(false);
                }
                setFarmsUserList(docs);
                setLoading(false);
            }
        });
    };

    const getFarmsSearchKey = (keyword) => {
        getAllFarmsByFarmer({ keyword, farmerId: userInformation.id, tokenValue }).then(
            (response) => {
                const { status, data } = response;

                if (status === 200) {
                    const { docs } = data;
                    if (docs.length === 0) {
                        setEmptyResult(true);
                    } else {
                        setEmptyResult(false);
                    }
                    setFarmsUserList(docs);
                    setLoading(false);
                }
            }
        );
    };

    const handleFilterChange = (event) => {
        setFilterText(event.target.value);
    };

    const onCloseDialog = () => {
        setOpenDialog(false);
    };

    useEffect(() => {
        if (debouncedSearchFarm) {
            getFarmsSearchKey(debouncedSearchFarm);
        } else {
            getFarmsByFarmer();
        }
    }, [debouncedSearchFarm]);

    return (
        <div>
            <div className="actions-farms mb-2"></div>

            <div className="">
                <div className="m-2">
                    <button className="blue-button flex" onClick={handleFarmRegister}>
                        <AgricultureRoundedIcon className="mr-1" />
                        Registrar finca
                    </button>
                </div>
                {/* IF THE DATA DIDN'T LOAD */}
                {loading ? (
                    <div className="flex-column justify-center text-center m-3 ">
                        <h2 className="text-lg font-bold">Cargando información...</h2>
                    </div>
                ) : (
                    <>
                        <div className="flex justify-center general-font m-3">
                            <TextField
                                className="text-field-search general-font"
                                label="Buscar finca"
                                value={filterText}
                                onChange={handleFilterChange}
                                InputProps={{
                                    startAdornment: (
                                        <InputAdornment position="start">
                                            <Search />
                                        </InputAdornment>
                                    )
                                }}
                                helperText="Buscar por nombre, ciudad, departamento"
                            />
                        </div>

                        <div className="flex justify-center">
                            <h2 className="text-lg font-bold">Lista de fincas</h2>
                        </div>

                        <div className="m-2">
                            <TableComponent
                                data={farmsUserList}
                                columns={columns}
                                onFarmView={handleViewFarm}
                            />
                        </div>

                        {!emptyResult ? null : (
                            <div>
                                <h1 className="mx-auto flex justify-center my-2">
                                    La busqueda no ha arrojado resultados...
                                </h1>
                            </div>
                        )}
                    </>
                )}
            </div>

            <DialogComponent
                inputsForm={inputsForm}
                openDialog={openDialog}
                dialogTitle={dialogTitle}
                onCloseDialog={onCloseDialog}
                labelForm={dialogFormLabel}
                onSubmit={onSubmitForm}
            />
        </div>
    );
};
