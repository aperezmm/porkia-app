import React, { useEffect, useState } from 'react';

//components
import { TableComponent } from '@/components/TableDynamic';

//hooks
import { useDebounce } from '@/hooks/useDebounce';

//material ui
import { TextField, InputAdornment } from '@mui/material';
import { Search } from '@mui/icons-material';
import { UpdatePigDialogComponent } from '@/components/Pig/UpdatePigDialogComponent';
import { EditDeletePigDialogComponent } from '@/components/Pig/EditDeletePigDialogComponent';
import { getStagesPig } from '@/services/Pig/PigManagement';

const columns = [
    // { key: 'id_pig', label: 'ID Cerdo' },
    // { key: 'id_batch', label: 'ID Lote' },
    { key: 'code', label: 'ID CERDO / LOTE' }
];

export const TableFilterFarmsComponent = ({ farmInformation, tokenValue }) => {

    const [openModalUpdatePig, setOpenModalUpdatePig] = useState(false);
    const [openModalEditDeletePig, setOpenModalEditDeletePig] = useState(false);
    const [pigsFarmInformation, setPigsFarmInformation] = useState(farmInformation.pigs);

    const [ selectedPig, setSelectedPig ] = useState({});
    const [ isFetchingLoading, setIsFetchingLoading] = useState(false);

    const [filterText, setFilterText] = useState('');
    const debouncedSearchFarm = useDebounce(filterText, 500);

    const handleUpdatePigBatch = (pig) => {

        setOpenModalUpdatePig(true);
        setIsFetchingLoading(true);
        console.log({ pig });
        getStagesPig({pigCode: pig._id, is_batch_record: pig.is_batch_record, tokenValue}).then((response) => {
            const { status, data } = response;
            setIsFetchingLoading(false);
            if(status === 200){
                const { response } = data;
                const pigStages = {...pig, stages: response};
                setSelectedPig(pigStages);
                
            }
        })
        
    };

    const handleCloseUpdatePigDialog = () => {
        setOpenModalUpdatePig(false);
    };

    const handleEditDeletePigBatch = (data) => {
        // console.log({ data });
        setOpenModalEditDeletePig(true);
    };

    const handleCloseEditDeletePigDialog = () => {
        setOpenModalEditDeletePig(false);
    };

    const handleFilterChange = (event) => {
        // console.log('cambiandoooo', event.target.value);
        setFilterText(event.target.value);
    };

    useEffect(() => {
        if (debouncedSearchFarm) {
        } else {
            // console.log('Traer todo');
        }
    }, [debouncedSearchFarm]);

    useEffect(() => {

        const addCodePigsFarmInformation = farmInformation.pigs.map((pig) => {
            if(pig.is_batch_record){
                return { ...pig, code: pig.id_batch }
            } else {
                return {...pig, code: pig.id_pig }
            }
        });

        setPigsFarmInformation(addCodePigsFarmInformation);

    },[pigsFarmInformation])

    return (
        <div id="table-filter" className="mt-1">
            <div className="flex justify-center general-font">
                <TextField
                    className="text-field-search general-font"
                    label="Buscar cerdo/lote"
                    value={filterText}
                    onChange={handleFilterChange}
                    InputProps={{
                        startAdornment: (
                            <InputAdornment position="start">
                                <Search />
                            </InputAdornment>
                        )
                    }}
                    helperText="Puede buscar por ID Cerdo / Lote"
                />
            </div>

            <TableComponent
                data={pigsFarmInformation}
                columns={columns}
                onEditDelete={handleEditDeletePigBatch}
                onUpdatePigBatch={handleUpdatePigBatch}
            />

            <UpdatePigDialogComponent
                openUpdatePigDialog={openModalUpdatePig}
                pig={selectedPig}
                isLoading={isFetchingLoading}
                onCloseUpdatePigDialog={handleCloseUpdatePigDialog}
            />

            <EditDeletePigDialogComponent
                openEditDeleteDialog={openModalEditDeletePig}
                onCloseEditDeleteDialog={handleCloseEditDeletePigDialog}
            />
        </div>
    );
};
