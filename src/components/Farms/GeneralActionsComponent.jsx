import React, { useEffect, useState } from 'react';

//services
import { addCauseDeath, addNewness, addBreed } from '@/services/User/UserManagement';
import { getInputs } from '@/utils/getInputs';
//component
import { DialogComponent } from '@/components/Farms/DialogComponent';
//hooks
import { useUser } from '@/hooks/useUser';
import { SweetAlert } from '@/components/Alerts/SweetAlert';
//materialui
import ArrowDropDownRoundedIcon from '@mui/icons-material/ArrowDropDownRounded';
import ArrowDropUpRoundedIcon from '@mui/icons-material/ArrowDropUpRounded';

export const GeneralActionsComponent = ({ updateBreeds }) => {
    //custom hook
    const { tokenValue, isAdmin, isTokenExpired, userInformation } = useUser();
    //breeds
    const [openBreed, setOpenBreed] = useState(false);
    //newness
    const [openNewness, setOpenNewness] = useState(false);
    //cause death
    const [openCauseDeath, setOpenCauseDeath] = useState(false);
    //forms formik states
    const [inputsForm, setInputsForm] = useState();
    //dialog states
    const [openDialog, setOpenDialog] = useState(false);
    const [dialogTitle, setDialogTitle] = useState('');
    const [dialogFormLabel, setDialogFormLabel] = useState('');
    const [keyForm, setKeyForm] = useState('');

    const handleFarmCauseDeath = () => {
        setOpenCauseDeath(false);
        setInputsForm(getInputs('farmAddCauseDeath'));
        setDialogTitle('Nueva causa de muerte');
        setDialogFormLabel('Agregar causa de muerte');
        setKeyForm('new_cause_death');
        setOpenDialog(true);
    };

    const handleShowCauseDeaths = () => {
        setOpenCauseDeath(false);
    };

    const handleAddNewness = () => {
        setOpenNewness(false);
        setInputsForm(getInputs('farmAddNewness'));
        setDialogTitle('Nueva novedad');
        setDialogFormLabel('Agregar novedad');
        setKeyForm('new_newness');
        setOpenDialog(true);
    };

    const handleShowNewness = () => {
        setOpenNewness(false);
    };

    const handleAddBreed = () => {
        setOpenBreed(false);
        setInputsForm(getInputs('farmAddBreed'));
        setDialogTitle('Nueva genética');
        setDialogFormLabel('Agregar genética');
        setKeyForm('new_breed');
        setOpenDialog(true);
    };

    const handleShowBreeds = () => {
        setOpenBreed(false);
    };

    //Dropdown buttons breed
    const handleOpenBreed = () => {
        setOpenBreed(!openBreed);
    };
    //Dropdown buttons newness
    const handleOpenNewness = () => {
        setOpenNewness(!openNewness);
    };
    //Dropdown buttons cause death
    const handleOpenCauseDeath = () => {
        setOpenCauseDeath(!openCauseDeath);
    };

    const onSubmitForm = (data) => {
        switch (keyForm) {
            case 'new_newness':
                addNewness({ data, tokenValue }).then((response) => {
                    const { status, data } = response;
                    if (status === 200) {
                        setOpenDialog(false);
                        SweetAlert({
                            title: 'Bien hecho',
                            text: data.message || 'Novedad registrada correctamente',
                            icon: 'success'
                        });
                    } else {
                        if (status === 409) {
                            SweetAlert({
                                title: 'Atención',
                                text: data.message || 'Novedad ya registrada',
                                icon: 'warning'
                            });
                        }
                    }
                });
                break;

            case 'new_breed':
                addBreed({ data, tokenValue }).then((response) => {
                    const { status, data } = response;
                    if (status === 200) {
                        setOpenDialog(false);
                        SweetAlert({
                            title: 'Bien hecho',
                            text: data.message || 'Genética registrada correctamente',
                            icon: 'success'
                        });
                        //Update the breeds
                        updateBreeds();
                    } else {
                        if (status === 409) {
                            SweetAlert({
                                title: 'Atención',
                                text: data.message || 'Genética ya registrada',
                                icon: 'warning'
                            });
                        }
                    }
                });
                break;

            case 'new_cause_death':
                addCauseDeath({ data, tokenValue }).then((response) => {
                    const { status, data } = response;
                    if (status === 200) {
                        setOpenDialog(false);
                        SweetAlert({
                            title: 'Bien hecho',
                            text: data.message || 'Causa de muerte registrada correctamente',
                            icon: 'success'
                        });
                    }
                });
                break;

            default:
                break;
        }
    };

    return (
        <>
            {/* Breeds */}
            <div className="dropdown">
                <button className="blue-button mx-1" onClick={() => handleOpenBreed()}>
                    Genéticas
                    {openBreed 
                        ? <ArrowDropUpRoundedIcon/>
                        : <ArrowDropDownRoundedIcon/>
                    }
                </button>
                {openBreed ? (
                    <ul className="menu">
                        <li className="menu-item">
                            <button className="green-button" onClick={() => handleAddBreed()}>
                                Nueva genética
                            </button>
                        </li>
                        {/* <li className="menu-item">
                            <button className="green-button" onClick={() => handleShowBreeds()}>
                                Ver genéticas
                            </button>
                        </li> */}
                    </ul>
                ) : null}
            </div>
            {/* Newness */}
            <div className="dropdown">
                <button className="blue-button mx-1" onClick={() => handleOpenNewness()}>
                    Novedades
                    {openNewness 
                        ? <ArrowDropUpRoundedIcon/>
                        : <ArrowDropDownRoundedIcon/>
                    }
                </button>
                {openNewness ? (
                    <ul className="menu">
                        <li className="menu-item">
                            <button className="green-button" onClick={() => handleAddNewness()}>
                                Nueva novedad
                            </button>
                        </li>
                        {/* <li className="menu-item">
                            <button className="green-button" onClick={() => handleShowNewness()}>
                                Ver novedades
                            </button>
                        </li> */}
                    </ul>
                ) : null}
            </div>
            {/* Cause of deaths */}
            <div className="dropdown">
                <button className="blue-button mx-1" onClick={() => handleOpenCauseDeath()}>
                    Causas de muerte
                    {openCauseDeath 
                        ? <ArrowDropUpRoundedIcon/>
                        : <ArrowDropDownRoundedIcon/>
                    }
                </button>
                {openCauseDeath ? (
                    <ul className="menu">
                        <li className="menu-item">
                            <button className="green-button" onClick={() => handleFarmCauseDeath()}>
                                Nueva causa
                            </button>
                        </li>
                        {/* <li className="menu-item">
                            <button
                                className="green-button"
                                onClick={() => handleShowCauseDeaths()}
                            >
                                Ver causas
                            </button>
                        </li> */}
                    </ul>
                ) : null}
            </div>

            <DialogComponent
                inputsForm={inputsForm}
                openDialog={openDialog}
                dialogTitle={dialogTitle}
                onCloseDialog={() => setOpenDialog(false)}
                labelForm={dialogFormLabel}
                onSubmit={onSubmitForm}
            />
        </>
    );
};
