import { Form, Formik, useFormikContext } from 'formik';

import { CustomCheckBox } from '@/components/CustomCheckbox';
import { CustomRadioGroup } from '@/components/CustomRadioGroup';
// import { CustomTextInput } from '@/components/CustomInput';
// import { CustomSelect } from '@/components/CustomSelect';
//Custom Material UI
import { CustomTextInput } from '@/components/MaterialUI/CustomInput';
import { CustomSelect } from '@/components/MaterialUI/CustomSelect';
import { CustomSelectAutocomplete } from './MaterialUI/CustomSelectAutocomplete';
import { LoadingDotsComponent } from '@/components/LoadingDotsComponent';
import { Grid } from '@mui/material';

export const FormikDynamic = ({
    inputs,
    initialValues,
    validationSchema,
    onSubmit,
    labelButton = 'Submit',
    disabledButton = false
}) => {

    const fieldWidth = (fullWidth, width) => {
        if (fullWidth) {
            return 12;
        } else if (width) {
            return width;
        } else {
            return 6;
        }
    };

    return (
        <Formik
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={async (values, { resetForm }) => {
                await onSubmit(values, { resetForm });
            }}
            enableReinitialize={true}
        >
            {({ resetForm, isValid }) => (
                <Form noValidate>
                    <Grid container>
                        {inputs.map(({ name, type, value, show = true, ...props }) => {
                            
                            if(show === false) return null;

                            switch (type) {
                                case 'autocomplete':
                                    return (
                                        <Grid
                                            item
                                            xs={12}
                                            md={fieldWidth(props.fullWidth, props.width)}
                                            key={name}
                                            className="p-2"
                                        >
                                            <CustomSelectAutocomplete
                                                key={name}
                                                id={name}
                                                label={props.label}
                                                name={name}
                                                options={props.options}
                                            />
                                        </Grid>
                                    );
                                case 'select':
                                    return (
                                        <Grid
                                            item
                                            xs={12}
                                            md={fieldWidth(props.fullWidth, props.width)}
                                            key={name}
                                            className="p-2"
                                        >
                                            <CustomSelect
                                                key={name}
                                                id={name}
                                                label={props.label}
                                                name={name}
                                                options={props.options}
                                            />
                                        </Grid>
                                    );

                                case 'radio-group':
                                    return (
                                        <Grid item xs={12} key={name} className="p-2">
                                            <CustomRadioGroup
                                                label={props.label}
                                                name={name}
                                                options={props.options}
                                                key={name}
                                            />
                                        </Grid>
                                    );

                                case 'checkbox':
                                    return (
                                        <Grid item xs={12} key={name} className="p-2">
                                            <CustomCheckBox
                                                label={props.label}
                                                key={name}
                                                name={name}
                                            />
                                        </Grid>
                                    );

                                default:
                                    return (
                                        <Grid
                                            item
                                            xs={12}
                                            key={name}
                                            className="p-2"
                                            md={fieldWidth(props.fullWidth, props.width)}
                                        >
                                            <CustomTextInput
                                                key={name}
                                                id={name}
                                                name={name}
                                                label={props.label}
                                                type={type}
                                                value={initialValues[name]}
                                                show={show}
                                                variant="outlined"
                                                // readOnly={true}
                                            />
                                        </Grid>
                                    );
                            }
                        })}
                    </Grid>
                    <button
                        className={`blue-button ${
                            disabledButton ? 'disabled:opacity-80 disabled:cursor-not-allowed' : ''
                        } `}
                        type="submit"
                        disabled={disabledButton}
                    >
                        {disabledButton ? <LoadingDotsComponent /> : labelButton }
                    </button>
                </Form>
            )}
        </Formik>
    );
};
