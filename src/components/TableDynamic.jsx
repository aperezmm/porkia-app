import React from 'react';
import {
    OpenInNewRounded,
    EditRounded,
    PasswordRounded
} from '@mui/icons-material';
import Tooltip from '@mui/material/Tooltip';
import LayersClearRoundedIcon from '@mui/icons-material/LayersClearRounded';

import {
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Paper
} from '@mui/material';

export const TableComponent = (props) => {
    const {
        data,
        columns,
        onUserEdit,
        onUserChangePassword,
        onFarmView,
        onEditDelete,
        onUpdatePigBatch,
        showActions = true
    } = props;

    return (
        <TableContainer component={Paper} className="mt-2">
            <Table aria-label="Dynamic table">
                <TableHead>
                    <TableRow>
                        {columns.map((column, index) => (
                            <TableCell key={index} className="general-font">
                                {column.label}{' '}
                            </TableCell>
                        ))}

                        {showActions && <TableCell className="general-font"> Acciones </TableCell>}
                    </TableRow>
                </TableHead>

                <TableBody>
                    {data.map((fila, index) => (
                        <TableRow key={index}>
                            {columns.map((column) => (
                                <TableCell
                                    key={`${fila.id}_${column.key}`}
                                    className="general-font"
                                >
                                    {fila[column.key]}
                                </TableCell>
                            ))}
                            <TableCell className="">
                                {/* If there is edit function, show button. */}
                                {onUserEdit && onUserChangePassword && (
                                    <div className="flex justify-between">
                                        <div className="w-fit">
                                            <Tooltip title="Editar usuario">
                                                <div
                                                    className="green-button general-font mx-1 general-font flex justify-between items-center w-full"
                                                    onClick={() => onUserEdit(fila)}
                                                >
                                                    <EditRounded />
                                                    Editar
                                                </div>
                                            </Tooltip>
                                        </div>

                                        <div className="w-fit">
                                            <Tooltip title="Cambiar contraseña">
                                                <div
                                                    className="green-button general-font mx-1 general-font flex justify-between items-center w-full"
                                                    onClick={() => onUserChangePassword(fila)}
                                                >
                                                    <PasswordRounded />
                                                    Cambiar contraseña
                                                </div>
                                            </Tooltip>
                                        </div>
                                    </div>
                                )}


                                {onFarmView && (
                                    <Tooltip title="Ver finca">
                                        <div
                                            className="green-button actions-list-farms general-font"
                                            onClick={() => onFarmView(fila)}
                                        >
                                            <OpenInNewRounded />
                                            Ver finca
                                        </div>
                                    </Tooltip>
                                )}

                                {onUpdatePigBatch && onEditDelete && (
                                    <div className="flex justify-between">
                                        <div className="w-fit">
                                            <Tooltip title="Actualización">
                                                <div
                                                    className="green-button general-font mx-1 general-font flex justify-between items-center w-full"
                                                    onClick={() => onUpdatePigBatch(fila)}
                                                >
                                                    <EditRounded />
                                                    Actualización
                                                </div>
                                            </Tooltip>
                                        </div>

                                        <div className="w-fit">
                                            <Tooltip title="Edición y eliminación">
                                                <div
                                                    className="green-button general-font mx-1 general-font flex justify-between items-center w-full"
                                                    onClick={() => onEditDelete(fila)}
                                                >
                                                    <LayersClearRoundedIcon />
                                                    Edición/Eliminación
                                                </div>
                                            </Tooltip>
                                        </div>
                                    </div>
                                )}
                            </TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    );
};
