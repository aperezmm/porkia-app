import React, { useEffect, useState, useContext } from 'react';
import { Formik, Form, Field, useFormikContext } from 'formik';
import { Button, TextField, MenuItem } from '@mui/material';
import * as Yup from 'yup';
//material ui
import { Grid } from '@mui/material';
//useContext
import { FarmViewPageContext } from '@/pages/Farm/FarmViewPage';

// Validación del esquema Yup
const validationSchema = Yup.object().shape({
    weight: Yup.number()
        .positive('El valor debe ser mayor a cero')
        .required('El campo es requerido'),
    consumption: Yup.number()
        .positive('El valor debe ser mayor a cero')
        .required('El campo es requerido'),
    nombre: Yup.string().required('El campo es requerido'),
    email: Yup.string().email('Email no válido').required('El campo es requerido'),
    materia: Yup.string()
        .oneOf(['materia1', 'materia2'], 'Materia no válida')
        .required('El campo es requerido')
});

const whitesmokeBackground = { backgroundColor: 'whitesmoke' };

export const UpdatePigFormulation = ({ pig }) => {
    const { farmDietsFormulated, farmInformation } = useContext(FarmViewPageContext);
    //formik context to get the values
    // const { values } =  useFormikContext();

    const [dietList, setDietList] = useState(farmDietsFormulated);
    const [stageList, setStageList] = useState([]);
    const [barnyardList, setBarnyardList] = useState([]);
    const [formFormik, setFormFormik] = useState({
        nombre: '',
        email: '',
        materia: '',
        diet: '',
        barnyard: '',
        stage: '',
        update_date: '',
        days_stage: 0,
        weight: 0,
        consumption: 0,
        gain: 0,
        gad: 0,
        conversion: 0
    });

    const getDaysDifference = (date1, date2) => {
        const oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
        const date1Time = new Date(date1).getTime();
        const date2Time = new Date(date2).getTime();
        const diffTime = date2Time - date1Time;
        const diffDays = Math.round(diffTime / oneDay);
        return diffDays;
    };

    const handleChangeForm = (field, value) => {
        const newFormFormik = { ...formFormik };

        newFormFormik[field] = value;

        // if(formFormik.stage){
        //     console.log("ya hay etapaaaa");
        // }

        console.log(`Field: ${field}, Value: ${value}`);

        if (field === 'update_date') {
            // console.log({pig});
            let days_stage;
            // si la etapa es destete calculamos con fecha de nacimiento
            if (newFormFormik.stage.order === 1) {
                days_stage = getDaysDifference(pig.born_date, Date.now());
                newFormFormik.days_stage = days_stage;
            } else {
            }
        }

        if (field === 'weight') {
            let gain;
            setFieldValue('weight', parseInt(value, 10));
            if (newFormFormik.stage.order === 1) {
                //calculate the gain
                gain = parseInt(newFormFormik.weight) - pig.birth_weight;
                newFormFormik.gain = gain;
                newFormFormik.gad = (gain / newFormFormik.days_stage).toFixed(3);
            } else {
            }
        }

        if (field === 'consumption') {
            let conversion;
            if (newFormFormik.stage.order === 1) {
                conversion =
                    parseInt(newFormFormik.consumption) / parseInt(newFormFormik.gain) || 0;

                newFormFormik.conversion = conversion;
            } else {
            }
        }

        // console.log('newFormFormik', newFormFormik);
        setFormFormik(newFormFormik);
    };

    useEffect(() => {
        console.log({ farmDietsFormulated });
        setDietList(farmDietsFormulated);
    }, [dietList]);

    useEffect(() => {
        if (farmInformation) {
            setStageList(farmInformation.stages);
            setBarnyardList(farmInformation.barnyard);
        }
    }, [farmInformation]);

    // useEffect(() => {
    //     console.log("formikContext", formikContext);
    // },[formikContext])

    return (
        <Formik
            initialValues={formFormik}
            validationSchema={validationSchema}
            onSubmit={(values) => {
                // console.log(values);
            }}
        >
            {({ errors, touched, setFieldValue }) => (
                <Form noValidate>
                    <div className="flex">
                        <div>
                            <Grid container>
                                <Grid item xs={12} md={12} className="p-2">
                                    <Field
                                        name="diet"
                                        as={TextField}
                                        select
                                        label="Asignar dieta"
                                        error={touched.diet && Boolean(errors.diet)}
                                        helperText={touched.diet && errors.diet}
                                        fullWidth
                                        value={formFormik.diet}
                                        onChange={(event) => {
                                            const { value } = event.target;
                                            handleChangeForm('diet', value);
                                        }}
                                        margin="normal"
                                    >
                                        {farmDietsFormulated.map((diet) => (
                                            <MenuItem key={diet._id} value={diet}>
                                                {diet.name}
                                            </MenuItem>
                                        ))}
                                    </Field>
                                </Grid>

                                <Grid item xs={12} md={6} className="p-2">
                                    <Field
                                        name="barnyard"
                                        as={TextField}
                                        select
                                        label="Corral"
                                        error={touched.barnyard && Boolean(errors.barnyard)}
                                        helperText={touched.barnyard && errors.barnyard}
                                        fullWidth
                                        value={formFormik.barnyard}
                                        onChange={(event) => {
                                            const { value } = event.target;
                                            handleChangeForm('barnyard', value);
                                        }}
                                        margin="normal"
                                    >
                                        {barnyardList.map((barnyard) => (
                                            <MenuItem key={barnyard._id} value={barnyard}>
                                                {barnyard.name}
                                            </MenuItem>
                                        ))}
                                    </Field>
                                </Grid>

                                <Grid item xs={12} md={6} className="p-2">
                                    <Field
                                        name="stage"
                                        as={TextField}
                                        select
                                        label="Etapa"
                                        error={touched.stage && Boolean(errors.stage)}
                                        helperText={touched.stage && errors.stage}
                                        fullWidth
                                        value={formFormik.stage}
                                        onChange={(event) => {
                                            const { value } = event.target;
                                            handleChangeForm('stage', value);
                                        }}
                                        margin="normal"
                                    >
                                        {stageList.map((stage) => (
                                            <MenuItem key={stage._id} value={stage}>
                                                {stage.name}
                                            </MenuItem>
                                        ))}
                                    </Field>
                                </Grid>

                                <Grid item xs={12} md={6} className="p-2">
                                    <Field
                                        name="update_date"
                                        as={TextField}
                                        type="date"
                                        label="Fecha actualización"
                                        error={touched.update_date && Boolean(errors.update_date)}
                                        helperText={touched.update_date && errors.update_date}
                                        fullWidth
                                        value={formFormik.update_date}
                                        onChange={(event) => {
                                            const { value } = event.target;
                                            handleChangeForm('update_date', value);
                                        }}
                                        margin="normal"
                                    />
                                </Grid>

                                <Grid item xs={12} md={6} className="p-2">
                                    <Field
                                        name="days_stage"
                                        as={TextField}
                                        label="Días en la etapa"
                                        error={touched.days_stage && Boolean(errors.days_stage)}
                                        helperText={touched.days_stage && errors.days_stage}
                                        value={formFormik.days_stage}
                                        disabled
                                        style={whitesmokeBackground}
                                        fullWidth
                                        margin="normal"
                                    />
                                </Grid>

                                <Grid item xs={12} md={6} className="p-2">
                                    <Field
                                        name="weight"
                                        as={TextField}
                                        type="number"
                                        label="Peso en [KG]"
                                        error={touched.weight && Boolean(errors.weight)}
                                        helperText={touched.weight && errors.weight}
                                        value={formFormik.weight}
                                        onChange={(event) => {
                                            const { value } = event.target;
                                            handleChangeForm('weight', parseInt(value, 10));
                                            setFieldValue('weight', parseInt(value, 10));                                            
                                        }}
                                        fullWidth
                                        margin="normal"
                                    />
                                </Grid>

                                <Grid item xs={12} md={6} className="p-2">
                                    <Field
                                        name="consumption"
                                        as={TextField}
                                        type="number"
                                        label="Consumo en [KG]"
                                        error={touched.consumption && Boolean(errors.consumption)}
                                        helperText={touched.consumption && errors.consumption}
                                        value={formFormik.consumption}
                                        onChange={(event) => {
                                            const { value } = event.target;
                                            console.log({ value });
                                            handleChangeForm('consumption', parseInt(value, 10));
                                            setFieldValue('consumption', parseInt(value, 10));
                                        }}
                                        fullWidth
                                        margin="normal"
                                    />
                                </Grid>

                                <Grid item xs={12} md={6} className="p-2">
                                    {/* ganancia */}
                                    <Field
                                        name="gain"
                                        as={TextField}
                                        type="number"
                                        label="Ganancia en [KG]"
                                        error={touched.gain && Boolean(errors.gain)}
                                        helperText={touched.gain && errors.gain}
                                        value={formFormik.gain}
                                        disabled
                                        style={whitesmokeBackground}
                                        fullWidth
                                        margin="normal"
                                    />
                                </Grid>

                                <Grid item xs={12} md={6} className="p-2">
                                    {/* GAD */}
                                    <Field
                                        name="gad"
                                        as={TextField}
                                        type="number"
                                        label="GAD en [KG]"
                                        error={touched.gad && Boolean(errors.gad)}
                                        helperText={touched.gad && errors.gad}
                                        value={formFormik.gad}
                                        disabled
                                        style={whitesmokeBackground}
                                        fullWidth
                                        margin="normal"
                                    />
                                </Grid>

                                <Grid item xs={12} md={6} className="p-2">
                                    {/* conversion */}
                                    <Field
                                        name="conversion"
                                        as={TextField}
                                        type="number"
                                        label="Conversión"
                                        error={touched.conversion && Boolean(errors.conversion)}
                                        helperText={touched.conversion && errors.conversion}
                                        value={formFormik.conversion}
                                        disabled
                                        style={whitesmokeBackground}
                                        fullWidth
                                        margin="normal"
                                    />
                                </Grid>

                                <Grid item xs={12} md={12}>
                                    {/* observations */}
                                    <Field
                                        name="observation"
                                        as={TextField}
                                        label="Observaciones"
                                        error={touched.observation && Boolean(errors.observation)}
                                        helperText={touched.observation && errors.observation}
                                        value={formFormik.observation}
                                        onChange={(event) => {
                                            const { value } = event.target;
                                            handleChangeForm('observation', value);
                                        }}
                                        fullWidth
                                        margin="normal"
                                    />
                                </Grid>
                            </Grid>
                        </div>

                        <div></div>
                    </div>

                    <button type="submit" className="green-button">
                        Enviar
                    </button>
                </Form>
            )}
        </Formik>
    );
};
