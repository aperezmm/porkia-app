import { Dialog, DialogTitle, DialogContent, IconButton } from '@mui/material';
import { Close } from '@mui/icons-material';
//COMPONENTS
import { SweetAlert } from '@/components/Alerts/SweetAlert';
import { UpdatePigFormulation } from '@/components/Pig/Formiks/UpdatePigFormik';
import { LoadingSpinner } from '../LoadingSpinnerComponent';

export const UpdatePigDialogComponent = ({
    dialogTitle = 'Actualizar cerdo/lote',
    openUpdatePigDialog,
    onCloseUpdatePigDialog,
    pig,
    isLoading
}) => {
    const handleCloseDialog = () => {
        onCloseUpdatePigDialog();
    };

    const onSubmitUpdatePig = () => {
        SweetAlert({
            title: '¡Atención!',
            text: 'Esto es un ejemplo de como será la navegación, los datos y calculos expuestos acá no son reales!',
            icon: 'warning'
        });
    };

    return (
        <Dialog open={openUpdatePigDialog} fullScreen className="p-10">
            <DialogTitle className="flex justify-between">
                <div className="mx-auto my-auto">
                    <h1 className="font-semibold">{dialogTitle}</h1>
                </div>
                <IconButton aria-label="close-icon" onClick={handleCloseDialog}>
                    <Close />
                </IconButton>
            </DialogTitle>
            <DialogContent className="custom-scrollbar">
                <div className="">
                    <div>
                        {isLoading 
                            ? <LoadingSpinner/>
                            : <UpdatePigFormulation pig={pig}/>
                        }
                    </div>
                </div>
            </DialogContent>
        </Dialog>
    );
};
