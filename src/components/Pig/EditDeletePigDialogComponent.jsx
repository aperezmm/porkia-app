import React, { useEffect, useState } from 'react';
import { Dialog, DialogTitle, DialogContent, IconButton } from '@mui/material';
import { Close } from '@mui/icons-material';

// import { EditDeletePigFormulation } from '@/components/Pig/Formiks/EditDeletePig';

import { FormikDynamic } from '@/components/FormikDynamic';
import { getInputs } from '@/utils/getInputs';
import { SweetAlert } from '@/components/Alerts/SweetAlert';

import { useFormikContext, Formik, Form, Field } from 'formik';

export const EditDeletePigDialogComponent = ({
    dialogTitle = 'Edición/Eliminación',
    openEditDeleteDialog,
    onCloseEditDeleteDialog
}) => {
    const handleCloseDialog = () => {
        onCloseEditDeleteDialog();
    };

    const onSubmitDeleteEditPig = () => {
        SweetAlert({
            title: '¡Atención!',
            text: 'Esto es un ejemplo de como será la navegación, los datos y calculos expuestos acá no son reales!',
            icon: 'warning'
        });
    };

    return (
        <Dialog open={openEditDeleteDialog} fullScreen className="p-10">
            <DialogTitle className="flex justify-between">
                <div className="mx-auto my-auto">
                    <h1 className="font-semibold">{dialogTitle}</h1>
                </div>
                <IconButton aria-label="close-icon" onClick={handleCloseDialog}>
                    <Close />
                </IconButton>
            </DialogTitle>
            <DialogContent className="custom-scrollbar">
                 <h1 className="font-semibold text-lg text-center">👷👷👷👷👷¡ESTA SECCIÓN ESTA EN CONSTRUCCIÓN!👷👷👷👷👷</h1>
                <div className="flex justify-between">
                    {/* <FormikDynamic {...getInputs('updatePig')} onSubmit={onSubmitDeleteEditPig} /> */}
                    <div className="w-full">
                        <h1 className="text-center">Información cerdo</h1>
                        <FormikDynamic
                            {...getInputs('editDeletePigSectionInformation')}
                            onSubmit={onSubmitDeleteEditPig}
                            labelButton=""
                        />
                        <FormikDynamic
                            {...getInputs('editDeletePigSectionForm')}
                            onSubmit={onSubmitDeleteEditPig}
                            labelButton=""
                        />
                        <div className="flex justify-around">
                            <button className="green-button" onClick={onSubmitDeleteEditPig}>
                                Editar
                            </button>
                            <button className="blue-button" onClick={onSubmitDeleteEditPig}>
                                Eliminar
                            </button>
                        </div>
                    </div>
                    <div className="w-full">
                        <h1 className="text-center">Sección de datos zootecnicos</h1>
                        <FormikDynamic
                            {...getInputs('editDeletePigSectionZootechnicDates')}
                            onSubmit={onSubmitDeleteEditPig}
                            labelButton=""
                        />
                    </div>
                    {/* <TwoFactorVerificationForm /> */}
                </div>
            </DialogContent>
        </Dialog>
    );
};

const AutoSubmitToken = () => {
    // Grab values and submitForm from context
    const { values, submitForm } = useFormikContext();
    useEffect(() => {
        // console.log('Listen changes', values);
        // Submit the form imperatively as an effect as soon as form values.token are 6 digits long
        //   if (values.token.length === 6) {
        //     submitForm();
        //   }
    }, [values, submitForm]);
    return null;
};

const TwoFactorVerificationForm = () => (
    <div>
        <h1>2-step Verification</h1>
        <p>Please enter the 6 digit code sent to your device</p>
        <Formik
            initialValues={{ token: '' }}
            validate={(values) => {
                const errors = {};
                if (values.token.length < 5) {
                    errors.token = 'Invalid code. Too short.';
                }
                return errors;
            }}
            onSubmit={(values, actions) => {
                setTimeout(() => {
                    alert(JSON.stringify(values, null, 2));
                    actions.setSubmitting(false);
                }, 1000);
            }}
        >
            <Form>
                <div className="border-2">
                    <Field name="token" type="tel" />
                </div>
                <AutoSubmitToken />
            </Form>
        </Formik>
    </div>
);
