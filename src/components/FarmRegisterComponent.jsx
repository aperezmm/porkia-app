import React, { useState, useEffect } from 'react';

import { Layout } from '@/components/Layout';
import { FormikDynamic } from '@/components/FormikDynamic';
import { getInputs } from '@/utils/getInputs';

//Material ui
import { AddCircleOutlineRounded } from '@mui/icons-material';
import { Tooltip } from '@mui/material';

//Services
import { createFarmAndAssignFarmer } from '@/services/Farm/FarmManagement';
import TokenService from '@/services/TokenService';
import { SweetAlert } from '@/components/Alerts/SweetAlert';

export const FarmRegisterComponent = () => {
    const [tokenService] = useState(TokenService());

    const [showRegisterComponent, setShowRegisterComponent] = useState(false);

    const onSubmitRegisterFarm = (values) => {
        const data = { ...values, farmer: tokenService.getTokenData().id };

        createFarmAndAssignFarmer({ data, tokenValue: tokenService.getToken() }).then(
            (response) => {
                const { status, data } = response;
                if (status === 200) {
                    SweetAlert({ title: '¡Bien hecho!', text: data.message, icon: 'success' });
                } else {
                    SweetAlert({ title: '¡Atención!', text: data.message || "Ha ocurrido un error al registrar la finca", icon: 'error' });
                }
            }
        );

    };

    return (
        <div id="farm-register-component">
            <h1>Vamos a crear la finca</h1>
            <Tooltip title="Registrar nueva finca">
                <AddCircleOutlineRounded
                    className="cursor-pointer"
                    fontSize="large"
                    onClick={consolelog}
                />
            </Tooltip>

            <Layout>
                <FormikDynamic {...getInputs('newFarm')} onSubmit={onSubmitRegisterFarm} />
            </Layout>
        </div>
    );
};
