import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
//material ui
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import Menu from '@mui/material/Menu';
import MenuIcon from '@mui/icons-material/MenuRounded';
import Container from '@mui/material/Container';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import Tooltip from '@mui/material/Tooltip';
import MenuItem from '@mui/material/MenuItem';
//constants
import { SESSION_STORAGE_KEYS } from '@/constants/sessionStorageKeys';
//hooks
import { useUser } from '@/hooks/useUser';

export function ResponsiveAppBar() {
    const navigate = useNavigate();

    const { tokenValue, isAdmin, isTokenExpired, userInformation } = useUser();

    console.log({userInformation});

    const [ pages, setPages ] = useState([]);
    const [ settings, setSettings ] = useState([]);

    const [anchorElNav, setAnchorElNav] = useState(null);
    const [anchorElUser, setAnchorElUser] = useState(null);

    const handleOpenNavMenu = (event) => {
        setAnchorElNav(event.currentTarget);
    };
    const handleOpenUserMenu = (event) => {
        setAnchorElUser(event.currentTarget);
    };

    const handleCloseNavMenu = () => {
        setAnchorElNav(null);
    };

    const handleCloseUserMenu = () => {
        setAnchorElUser(null);
    };
    //click on nav menu
    const handleMenuClickNav = (key) => {
        switch (key) {
            case 'home':
                if(!tokenValue){
                    navigate('/login');
                } else if(isAdmin){
                    navigate('/main/admin');
                } else {
                    navigate('/main/user');
                }
                break;

            case 'users':
                navigate('/main/admin/users');
                break;
            
            case 'farms':
                navigate('/main/admin/farms')
                break;

            case 'raw-materials':
                navigate('/main/raw-materials');
                break;

            default:
                break;
        }
    };

    //Logout, and remove session storage keys
    const handleLogout = () => {
        sessionStorage.removeItem(SESSION_STORAGE_KEYS.ACCESS_TOKEN);
        navigate('/login');
    };

    //click on menu
    const handleMenuClick = (key) => {
        switch (key) {
            case 'logout':
                setAnchorElUser(null);
                handleLogout();
                break;

            case 'perfil':
                setAnchorElUser(null);
                break;

            case 'change-password':
                setAnchorElUser(null);
                navigate('/main/user/profile/change-password');
                break;

            default:
                break;
        }
    };

    useEffect(() => {
        if(!tokenValue){
            return;
        } else if(isAdmin) {
            setPages([
                { key: 'home', label: 'Dashboard' },
                {key: 'farms', label: 'Fincas'},
                { key: 'users', label: 'Usuarios'},
                { key: 'raw-materials', label: 'Materias prima' },
            ])

            setSettings([
                { key: 'change-password', label: 'Cambiar contraseña' },
                { key: 'logout', label: 'Cerrar sesión' }
            ]);

        } else {
            //then is user
            setPages([
                { key: 'home', label: 'Dashboard' },
                { key: 'raw-materials', label: 'Materias primas'}
            ]);

            setSettings([
                { key: 'change-password', label: 'Cambiar contraseña' },
                { key: 'logout', label: 'Cerrar sesión' }
            ]);
        }
    },[])

    return (
        <AppBar
            position="fixed"
            className="general-font"
            sx={{ backgroundColor: '#013284' }} //backdropFilter: 'blur(10px)'
        >
            <Container maxWidth="xl">
                <Toolbar disableGutters>

                    {/* this title just when the user isn't logged */}
                    { !tokenValue ?
                        <Typography
                            variant="h5"
                            noWrap
                            component="a"
                            href=""
                            className="general-font "
                            sx={{
                                mr: 8,
                                display: { xs: 'flex' },
                                justifyContent: 'center',
                                flexGrow: 1,
                                fontFamily: 'general-font',
                                fontWeight: 800,
                                fontSize: 'xxx-large',
                                width: '100%',
                                textDecoration: 'none'
                            }}
                        >
                            Pork IA
                        </Typography> : null 
                    } 


                    { tokenValue && 
                        <Box sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}>
                            <IconButton
                                size="large"
                                aria-label="account of current user"
                                aria-controls="menu-appbar"
                                aria-haspopup="true"
                                onClick={handleOpenNavMenu}
                                color="white"
                            >
                                <MenuIcon sx={{ color: "#fff" }}/>
                            </IconButton>
                            <Menu
                                id="menu-appbar"
                                anchorEl={anchorElNav}
                                anchorOrigin={{
                                    vertical: 'bottom',
                                    horizontal: 'left'
                                }}
                                keepMounted
                                transformOrigin={{
                                    vertical: 'top',
                                    horizontal: 'left'
                                }}
                                open={Boolean(anchorElNav)}
                                onClose={handleCloseNavMenu}
                                sx={{
                                    display: { xs: 'block', md: 'none' },
                                    color: 'white'
                                }}
                            >
                                {pages.map((page) => (
                                    <MenuItem
                                        key={page.key}
                                        onClick={() => handleMenuClickNav(page.key)}
                                    >
                                        <Typography textAlign="center" >
                                            {page.label}
                                        </Typography>
                                    </MenuItem>
                                ))}
                            </Menu>
                        </Box> 
                    }

                    {tokenValue &&
                        <div className="w-full flex justify-center md:w-auto">
                            <Avatar alt="" src="/porkia/white-icon.png" style={{ width: 'auto' }} />
                        </div>
                    }

                    <Box sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' } }}>
                        {pages.map((page) => (
                            <Button
                                key={page.key}
                                onClick={() => handleMenuClickNav(page.key)}
                                sx={{ my: 2, color: 'white', display: 'block' }}
                                className="general-font"
                            >
                                {page.label}
                            </Button>
                        ))}
                    </Box>
                    {/* if the user is logged then show */}
                    {tokenValue ?
                        <Box sx={{ flexGrow: 0 }}>
                            <Tooltip title="Menú">
                                <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
                                    <Avatar alt={userInformation.name} src="/static/images/avatar/2.jpg" />
                                </IconButton>
                            </Tooltip>
                            <Menu
                                sx={{ mt: '45px' }}
                                id="menu-appbar"
                                anchorEl={anchorElUser}
                                anchorOrigin={{
                                    vertical: 'top',
                                    horizontal: 'right'
                                }}
                                keepMounted
                                transformOrigin={{
                                    vertical: 'top',
                                    horizontal: 'right'
                                }}
                                open={Boolean(anchorElUser)}
                                onClose={handleCloseUserMenu}
                            >
                                {settings.map((setting) => (
                                    <MenuItem
                                        key={setting.key}
                                        onClick={() => handleMenuClick(setting.key)}
                                    >
                                        <Typography textAlign="center">{setting.label}</Typography>
                                    </MenuItem>
                                ))}
                            </Menu>
                        </Box>
                        : null
                    }
                </Toolbar>
            </Container>
        </AppBar>
    );
}
