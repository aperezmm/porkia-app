import { useState, useCallback} from "react";

import TokenService from "@/services/TokenService";
import jwtDecoder from "jwt-decode";

export function useUser(){
    const [tokenService] = useState(TokenService());
    const tokenValue = tokenService.getToken();
    const userRole = tokenService.getUserRole();
    

    const isAdmin = useCallback(() => {
        if(userRole === 'ADMIN'){
            return true;
        }else{
            return false;
        }
    },[userRole])

    const isTokenExpired = useCallback(() => {
        if(tokenValue){
            let decodedToken = jwtDecoder(tokenValue);
            let currentDate = new Date();

            if(decodedToken.exp * 1000 < currentDate.getTime()){
                return true;
            }
            return false;
        }
        return false;
        
    },[tokenValue])

    const userInformation = useCallback(() => {
        if(tokenValue){
            let decodedToken = jwtDecoder(tokenValue);
            return decodedToken;
        } else {
            return {};
        }
    },[tokenValue])

    return {
        tokenValue, 
        isAdmin: isAdmin(),
        isTokenExpired: isTokenExpired(),
        userInformation: userInformation()
    };
}