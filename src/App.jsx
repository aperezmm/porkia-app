import './App.css';

import { BrowserRouter, Routes, Route, Outlet, Navigate } from 'react-router-dom';

import { lazy, Suspense } from 'react';
//PAGES
// import { LoginPage } from '@/pages/Auth/LoginPage';
const LoginPage = lazy(() => import('@/pages/Auth/LoginPage'));
import { UsersPage } from '@/pages/Profile/Admin/UsersPage';
import { FarmsPage } from '@/pages/Profile/Admin/FarmsPage';
import { OverviewPage } from '@/pages/Overview/OverviewPage';
import { FarmViewPage } from '@/pages/Farm/FarmViewPage';
import { MainRedirect } from '@/layouts/MainRedirect';
import { OverviewPageAdmin } from '@/pages/Overview/OverviewPageAdmin';
import { ChangePasswordPage } from '@/pages/Auth/ChangePassword';
import { RawMaterialPage } from '@/pages/RawMaterial.jsx/RawMaterialPage';
import { ForgotPasswordPage } from '@/pages/Auth/ForgotPassword';
import { NotFoundPage } from '@/pages/NotFound/NotFoundPage';

//COMPONENTS
import { ResponsiveAppBar } from '@/components/AppBarMenuComponent';
//HOOKS
import { useUser } from '@/hooks/useUser';
import { LoadingSpinner } from '@/components/LoadingSpinnerComponent';

function App() {
    //This function is used to protected the routes and separate between admin and user
    const ProtectedRoute = ({ children, redirectPath = '/login' }) => {
        const { tokenValue, isTokenExpired } = useUser();

        if (!tokenValue || isTokenExpired) {
            return <Navigate to={redirectPath} replace />;
        }

        return children ? children : <Outlet />;
    };

    const BASE_URL = import.meta.env.VITE_REACT_APP_BASE_URL;

    return (
        <BrowserRouter basename='/porkia'>
            <Suspense fallback={<LoadingSpinner/>}>

            
            <Routes>
                <Route path="/" element={<Navigate to="/login" replace />} />

                <Route
                    path="/login"
                    element={
                        <>
                            <ResponsiveAppBar />
                            <LoginPage />
                        </>
                    }
                />

                <Route
                    path="/forget-password"
                    element={
                        <>
                            <ResponsiveAppBar />
                            <ForgotPasswordPage />
                        </>
                    }
                />

                {/* Protected multiple routes */}
                <Route element={<ProtectedRoute />}>
                    <Route path="/main" element={<MainRedirect />} />
                    {/* ADMIN ROUTES */}
                    <Route
                        path="/main/admin/users"
                        element={
                            <>
                                <ResponsiveAppBar />
                                <UsersPage />
                            </>
                        }
                    />

                    <Route
                        path="/main/admin/farms"
                        element={
                            <>
                                <ResponsiveAppBar />
                                <FarmsPage />
                            </>
                        }
                    />

                    <Route
                        path="/main/admin"
                        element={
                            <>
                                <ResponsiveAppBar />
                                <OverviewPageAdmin />
                            </>
                        }
                    />

                    {/* -------------------------------------- */}
                    {/* USER ROUTES */}
                    <Route
                        path="/main/user"
                        element={
                            <>
                                <ResponsiveAppBar />
                                <OverviewPage />
                            </>
                        }
                    />

                    <Route
                        path="/main/user/profile/change-password"
                        element={
                            <>
                                <ResponsiveAppBar />
                                <ChangePasswordPage />
                            </>
                        }
                    />

                    <Route
                        path="/main/user/farms/:farmId"
                        element={
                            <>
                                <ResponsiveAppBar />
                                <FarmViewPage />
                            </>
                        }
                    />

                    <Route
                        path="/main/raw-materials"
                        element={
                            <>
                                <ResponsiveAppBar />
                                <RawMaterialPage />
                            </>
                        }
                    />
                </Route>

                <Route path="*" element={<NotFoundPage />} />
            </Routes>

            </Suspense>
        </BrowserRouter>
    );
}

export default App;
