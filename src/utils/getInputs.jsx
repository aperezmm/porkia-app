import * as Yup from "yup";
//Imports the forms
import { forms } from "@/utils/forms";

const generateValidations = (field) => {

    //Check the type of validation of field
    let schema = Yup[field.typeValue ? field.typeValue : 'string']();

    for (const rule of field.validations) {
        switch (rule.type) {
            case 'isTrue': schema = (schema).isTrue(rule.message); break;
            case 'isEmail': schema = (schema).email(rule.message); break;
            case 'minLength': schema = (schema).min(rule?.value, rule.message); break;
            case 'maxLength': schema = (schema).max(rule?.value, rule.message); break;
            // Check should numeric field
            case 'isNumeric': schema = schema.test('isNumeric', rule.message, (value) => {
                return /^[0-9]+$/.test(value); // Validar si el valor es numérico
            }); break;
            case 'matchesField': schema = schema.test('matchesField', rule.message, (value, context) => {
                return value === context.parent[rule.field];
            }); break;
            default: schema = schema.required(rule.message); break;
        }
    }

    return schema
}


export const getInputs = (section) => {

    let initialValues = {};

    let validationsFields = {};

    for (const field of forms[section]) {

        initialValues[field.name] = field.value;

        if (!field.validations) continue;

        const schema = generateValidations(field)

        validationsFields[field.name] = schema;
    }

    return {
        validationSchema: Yup.object({ ...validationsFields }),
        initialValues,
        inputs: forms[section],
    };

};