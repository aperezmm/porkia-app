/**
 * It returns an array of objects, each object representing an input field
 */
const LoginForm = () => {
    return [
        {
            type: 'text',
            name: 'logInKey',
            label: 'Nombre de usuario o identificación',
            value: '',
            width: 12,
            validations: [
                {
                    type: 'minLength',
                    value: 3,
                    message: 'Min. 3 caracteres'
                },
                {
                    type: 'required',
                    message: 'El campo es requerido'
                }
            ]
        },
        {
            type: 'password',
            name: 'password',
            label: 'Contraseña',
            width: 12,
            value: '',
            validations: [
                {
                    type: 'required',
                    message: 'La campo es requerido'
                }
            ]
        }
    ];
};


const ForgotPassworLogInKeyForm = () => {
    return [
        {
            type: 'text',
            name: 'logInKey',
            label: 'Usuario o identificación',
            value: '',
            width: 12,
            validations: [
                {
                    type: 'required',
                    message: 'El campo es requerido'
                }
            ]
        }
    ]
}


const ForgotPasswordForm = () => {
    return [
        {
            type: 'text',
            name: 'newPassword',
            label: 'Nueva contraseña',
            value: '',
            width: 12,
            validations: [
                {
                    type: 'required',
                    message: 'El campo es requerido'
                },
                {
                    type: 'matchesField',
                    field: 'confirmedPassword',
                    message: 'Los campos deben ser iguales'
                }
            ]
        },
        {
            type: 'text',
            name: 'confirmedPassword',
            label: 'Confirmar contraseña',
            value: '',
            width: 12,
            validations: [
                {
                    type: 'required',
                    message: 'El campo es requerido'
                },
                {
                    type: 'matchesField',
                    field: 'newPassword',
                    message: 'Los campos deben ser iguales'
                }
            ]
        }
    ]
}

const RegisterUserForm = () => {
    return [
        {
            type: 'text',
            name: 'name',
            label: 'Nombres',
            value: '',
            validations: [
                {
                    type: 'required',
                    message: 'El campo es requerido'
                }
            ]
        },
        {
            type: 'text',
            name: 'surname',
            label: 'Apellidos',
            value: '',
            validations: [
                {
                    type: 'required',
                    message: 'El campo es requerido'
                }
            ]
        },
        {
            type: 'email',
            name: 'email',
            label: 'Correo electronico',
            width: 12,
            value: '',
            validations: [
                {
                    type: 'required',
                    message: 'El campo es requerido'
                },
                {
                    type: 'isEmail',
                    message: 'El correo eletronico es invalido'
                }
            ]
        },
        {
            type: 'text',
            name: 'country',
            label: 'País',
            value: ''
        },
        {
            type: 'text',
            name: 'cellphone',
            label: 'Celular',
            value: '',
            validations: [
                {
                    type: 'required',
                    message: 'El campo es requerido'
                },
                {
                    type: 'isNumeric',
                    message: 'El campo debe ser númerico'
                },
                {
                    type: 'minLength',
                    value: 10,
                    message: 'El campo debe tener mínimo 10 números'
                }
            ]
        },
        {
            type: 'text',
            name: 'about_me',
            label: 'Acerca de mí',
            width: 12,
            value: '',
            validations: [
                {
                    type: 'maxLength',
                    value: 100,
                    message: "El campo debe tener maximo 100 caracteres"
                }
            ]
        },
        {
            type: 'text',
            name: 'username',
            label: 'Nombre de usuario',
            value: '',
            validations: [
                {
                    type: 'required',
                    message: "El campo es requerido"
                }
            ]
        },     
        {
            type: 'text',
            name: 'password',
            label: 'Contraseña',
            value: '',
            validations: [
                // {
                //     type: 'minLength',
                //     value: 8,
                //     message: 'Min. 8 caracteres'
                // }
            ]
        },
        {
            type: 'text',
            name: 'identification',
            label: 'Identificación',
            value: '',
            validations: [
                {
                    type: 'required',
                    message: 'El campo es requerido'
                },
                {
                    type: 'minLength',
                    value: 4,
                    message: 'Min. 4 caracteres'
                }
            ]
        },
        {
            type: 'select',
            name: 'roles',
            label: 'Roles',
            value: '',
            options: [
                {
                    value: 'USER',
                    label: 'Usuario'
                },
                {
                    value: 'ADMIN',
                    label: 'Administrador'
                }
            ],
            validations: [
                {
                    type: 'required',
                    message: 'Debe seleccionar un ROL para el perfil'
                }
            ]
        }
    ];
};

const UpdateUserPasswordForm = () => {
    return [
        {
            type: 'text',
            name: 'password',
            label: 'Contraseña actual',
            value: '',
            width: 12,
            show: false,
            validations: [
                {
                    type: 'required',
                    message: 'El campo es requerido'
                },
            ]
        },
        {
            type: 'text',
            name: 'newPassword',
            label: 'Nueva contraseña',
            width: 12,
            value: '',
            validations: [
                {
                    type: 'required',
                    message: 'El campo es requerido'
                },
                {
                    type: 'matchesField',
                    field: 'confirmedPassword',
                    message: 'Los campos deben ser iguales'
                }
            ]
        },
        {
            type: 'text',
            name: 'confirmedPassword',
            label: 'Confirmar contraseña',
            width: 12,
            value: '',
            validations: [
                {
                    type: 'required',
                    message: 'El campo es requerido'
                },
                {
                    type: 'matchesField',
                    field: 'newPassword',
                    message: 'Los campos deben ser iguales'
                }
            ]
        }
    ];
};

const UpdateUserForm = () => {
    return [
        {
            type: 'text',
            name: 'name',
            label: 'Nombre',
            value: '',
            validations: [
                {
                    type: 'required',
                    message: 'El campo es requerido'
                }
            ]
        },
        {
            type: 'text',
            name: 'surname',
            label: 'Apellido',
            value: '',
            validations: [
                {
                    type: 'required',
                    message: 'El campo es requerido'
                }
            ]
        },
        {
            type: 'text',
            name: 'email',
            label: 'Email',
            width: 12,
            value: '',
            validations: [
                {
                    type: 'required',
                    message: 'El campo es requerido'
                },
                {
                    type: 'isEmail',
                    message: 'El correo eletronico es invalido'
                }
            ]
        },
        {
            type: 'text',
            name: 'country',
            label: 'País',
            value: ''
        },
        {
            type: 'text',
            name: 'cellphone',
            label: 'Celular',
            value: '',
            validations: [
                {
                    type: 'required',
                    message: 'El campo es requerido'
                },
                {
                    type: 'isNumeric',
                    message: 'El campo debe ser númerico'
                }
            ]
        },
        {
            type: 'text',
            name: 'about_me',
            label: 'Acerca de mí',
            width: 12,
            value: '',
            validations: [
                {
                    type: 'maxLength',
                    value: 100,
                    message: "El campo debe tener maximo 100 caracteres"
                }
            ]
        },
    ];
};

const NewFarmFormAdmin = () => {
    return [
        {
            type: 'text',
            name: 'name',
            label: 'Nombre de la finca',
            value: '',
            width: 12,
            validations: [
                {
                    type: 'required',
                    message: 'El campo es requerido'
                }
            ]
        },
        {
            type: 'select',
            name: 'farmer',
            label: 'Administrador',
            width: 12,
            value: '',
            options: []
        },
        {
            type: 'text',
            name: 'country',
            label: 'Pais',
            value: '',
            validations: [
                {
                    type: 'required',
                    message: 'El campo es requerido'
                }
            ]
        },
        {
            type: 'text',
            name: 'department',
            label: 'Departamento',
            value: '',
            validations: [
                {
                    type: 'required',
                    message: 'El campo es requerido'
                }
            ]
        },
        {
            type: 'text',
            name: 'city',
            label: 'Ciudad',
            width: 12,
            value: '',
            validations: [
                {
                    type: 'required',
                    message: 'El campo es requerido'
                }
            ]
        }
    ];
};

const NewFarmForm = () => {
    return [
        {
            type: 'text',
            name: 'name',
            label: 'Nombre de la finca',
            width: 12,
            value: '',
            validations: [
                {
                    type: 'required',
                    message: 'El campo es requerido'
                }
            ]
        },
        {
            type: 'text',
            name: 'country',
            label: 'Pais',
            value: '',
            validations: [
                {
                    type: 'required',
                    message: 'El campo es requerido'
                }
            ]
        },
        {
            type: 'text',
            name: 'department',
            label: 'Departamento',
            value: '',
            validations: [
                {
                    type: 'required',
                    message: 'El campo es requerido'
                }
            ]
        },
        
        {
            type: 'text',
            name: 'city',
            label: 'Ciudad',
            width: 12,            
            value: '',
            validations: [
                {
                    type: 'required',
                    message: 'El campo es requerido'
                }
            ]
        }
    ];
};

const FarmAddStage = () => {
    return [
        {
            type: 'text',
            name: 'name',
            label: 'Nombre etapa',
            width: 12,
            value: '',
            validations: [
                {
                    type: 'required',
                    message: 'El campo es requerido'
                }
            ]
        },
        {
            type: 'text',
            name: 'order',
            label: 'Orden en las etapas',
            width: 12,
            value: '',
            validations: [
                {
                    type: 'required',
                    message: 'El campo es requerido'
                },
                {
                    type: 'isNumeric',
                    message: 'El campo debe ser númerico'
                }
            ]
        }
    ];
};

const FarmAddCauseDeath = () => {
    return [
        {
            type: 'text',
            name: 'name',
            label: 'Nombre causa de muerte',
            width: 12,
            value: '',
            validations: [
                {
                    type: 'required',
                    message: 'El campo es requerido'
                }
            ]
        }
    ];
};

const FarmAddNewness = () => {
    return [
        {
            type: 'text',
            name: 'name',
            label: 'Nombre novedad',
            width: 12,
            value: '',
            validations: [
                {
                    type: 'required',
                    message: 'El campo es requerido'
                }
            ]
        }
    ];
};

const FarmAddBreed = () => {
    return [
        {
            type: 'text',
            name: 'name',
            label: 'Nombre genética',
            width: 12,
            value: '',
            validations: [
                {
                    type: 'required',
                    message: 'El campo es requerido'
                }
            ]
        }
    ];
};

const FarmAddBarnyard = () => {
    return [
        {
            type: 'text',
            name: 'name',
            label: 'Nombre corral',
            width: 12,
            value: '',
            validations: [
                {
                    type: 'required',
                    message: 'El campo es requerido'
                }
            ]
        },
        {
            type: 'text',
            name: 'area',
            label: 'Area en metros (m2)',
            width: 12,
            value: '',
            validations: [
                {
                    type: 'required',
                    message: 'El campo es requerido'
                },
                {
                    type: 'isNumeric',
                    message: 'El campo debe ser númerico'
                }
            ]
        }
    ];
};

/* --------- */

const NewPigBatchForm = () => {
    return [
        {
            type: 'text',
            name: 'is_batch_record',
            label: '¿Es registro de lote?',
            value: true,
            show: false,
            validations: []
        },
        //id batch
        {
            type: 'text',
            name: 'id_batch',
            label: 'ID Lote',
            value: '',
            validations: [
                {
                    type: 'required',
                    message: 'El campo es requerido'
                }
            ]
        },
        //ID mom
        {
            type: 'text',
            name: 'id_mom',
            label: 'ID madre',
            value: ''
        },
        {
            type: 'select',
            name: 'gender',
            label: 'Genero',
            width: 12,
            value: '',
            options: [
                {
                    value: 'macho',
                    label: 'Macho'
                },
                {
                    value: 'hembra',
                    label: 'Hembra'
                },
                {
                    value: 'mixto',
                    label: 'Mixto'
                }
            ]
        },
        {
            type: 'autocomplete',
            name: 'breed',
            label: 'Genética',
            width: 12,
            value: '',
            options: [],
            validations: [
                {
                    type: 'required',
                    message: 'El campo es requerido'
                }
            ]
        },
        {
            type: 'text',
            name: 'quantity_males',
            label: 'Cantidad de machos',
            value: 0,
            validations: [
                {
                    type: 'required',
                    message: 'El campo es requerido'
                },
                {
                    type: 'isNumeric',
                    message: 'El campo debe ser númerico'
                }
            ]
        },
        {
            type: 'text',
            name: 'male_percentage',
            label: 'Porcentaje de machos',
            value: 0,
            show: false
        },
        {
            type: 'text',
            name: 'initial_batch_size',
            label: 'Tamaño de lote inicial',
            value: 0,
            validations: [
                {
                    type: 'required',
                    message: 'El campo es requerido'
                },
                {
                    type: 'isNumeric',
                    message: 'El campo debe ser númerico'
                }
            ]
        },
        {
            type: 'text',
            name: 'final_batch_size',
            label: 'Tamaño de lote final',
            value: 0,
            validations: [
                {
                    type: 'required',
                    message: 'El campo es requerido'
                },
                {
                    type: 'isNumeric',
                    message: 'El campo debe ser númerico'
                }
            ]
        },
        {
            type: 'date',
            name: 'born_date',
            label: 'Fecha de nacimiento',
            value: '',
            validations: [
                {
                    type: 'required',
                    message: 'El campo es requerido'
                }
            ]
        },
        {
            type: 'text',
            name: 'birth_weight',
            label: 'Peso de lote [KG]',
            value: 0,
            validations: [
                {
                    type: 'required',
                    message: 'El campo es requerido'
                }
            ]
        },
        {
            type: 'select',
            name: 'clasification',
            label: 'Clasificación',
            value: '',
            options: [
                {
                    value: 'cabeza',
                    label: 'Cabeza'
                },
                {
                    value: 'medios',
                    label: 'Medios'
                },
                {
                    value: 'colas',
                    label: 'Colas'
                },
                {
                    value: 'mixto',
                    label: 'Mixto'
                }
            ]
        },
        {
            type: 'text',
            name: 'observations',
            label: 'Novedad',
            width: 12,
            value: '',
            validations: [
                {
                    type: "maxLength",
                    value: 100,
                    message: "Maximo 100 caracteres"
                }
            ]
        }
    ];
};

const NewPigForm = () => {
    return [
        {
            type: 'text',
            name: 'is_batch_record',
            label: '¿Es registro de lote?',
            value: false,
            show: false,
            validations: []
        },
        {
            type: 'text',
            name: 'id_pig',
            label: 'ID Cerdo',
            value: '',
            validations: [
                {
                    type: 'required',
                    message: 'El campo es requerido'
                }
            ]
        },
        //ID mom
        {
            type: 'text',
            name: 'id_mom',
            label: 'ID madre',
            value: ''
        },
        {
            type: 'select',
            name: 'gender',
            label: 'Genero',
            width: 12,
            value: '',
            options: [
                {
                    value: 'macho',
                    label: 'Macho'
                },
                {
                    value: 'hembra',
                    label: 'Hembra'
                }
            ],
            validations: [
                {
                    type: 'required',
                    message: 'El campo es requerido'
                }
            ]
        },
        {
            type: 'autocomplete',
            name: 'breed',
            label: 'Genética',
            width: 12,
            value: '',
            options: [],
            validations: [
                {
                    type: 'required',
                    message: 'El campo es requerido'
                }
            ]
        },
        {
            type: 'date',
            name: 'born_date',
            label: 'Fecha de nacimiento',
            value: '',
            validations: [
                {
                    type: 'required',
                    message: 'El campo es requerido'
                }
            ]
        },
        {
            type: 'text',
            name: 'birth_weight',
            label: 'Peso al nacer [KG]',
            value: 0,
            validations: [
                {
                    type: 'required',
                    message: 'El campo es requerido'
                },
                {
                    type: 'isNumeric',
                    message: 'El campo debe ser numerico'
                }
            ]
        },
        {
            type: 'select',
            name: 'clasification',
            label: 'Clasificación',
            width: 12,
            value: '',
            options: [
                {
                    value: 'cabeza',
                    label: 'Cabeza'
                },
                {
                    value: 'medios',
                    label: 'Medios'
                },
                {
                    value: 'colas',
                    label: 'Colas'
                },
                {
                    value: 'mixto',
                    label: 'Mixto'
                }
            ]
        },
        {
            type: 'text',
            name: 'observations',
            label: 'Novedad',
            width: 12,
            value: '',
            validations: [
                {
                    type: 'maxLength',
                    value: 100,
                    message: 'Max. 100 caracteres'
                }
            ]
        },
        {
            type: 'text',
            name: 'initial_batch_size',
            label: 'Tamaño de lote inicial',
            value: 1,
            show: false
        },
    ];
};

const NewRawMaterial = () => {
    return [
        {
            type: 'text',
            name: 'name',
            label: 'Nombre de la materia',
            width: 12,
            value: '',
            validations: [
                {
                    type: 'maxLength',
                    value: 50,
                    message: 'Max. 50 caracteres'
                }
            ]
        }
    ]
}

const UpdatePigSectionZootechnicDatesForm = () => {
    return [
        {
            type: 'select',
            name: 'diet',
            label: 'Dieta asignada',
            value: '',
            options: [
                {
                    value: 2537,
                    label: 'Dieta 1'
                },
                {
                    value: 2538,
                    label: 'Dieta 2'
                },
                {
                    value: 2539,
                    label: 'Dieta 3'
                }
            ],
        },
        {
            type: 'select',
            name: 'barnyard',
            label: 'Corral',
            value: '',
            options: [
                {
                    value: 2537,
                    label: 'Corral 1'
                },
                {
                    value: 2538,
                    label: 'Corral 2'
                },
                {
                    value: 2539,
                    label: 'Corral 3'
                }
            ],
        },
        {
            type: 'select',
            name: 'stage',
            label: 'Etapa',
            value: '',
            options: [
                {
                    value: 2537,
                    label: 'Etapa 1'
                },
                {
                    value: 2538,
                    label: 'Etapa 2'
                },
                {
                    value: 2539,
                    label: 'Etapa 3'
                }
            ],
        },
        {
            type: 'text',
            name: 'stages_days',
            label: 'Días en la etapa',
            value: '15'
        },
        {
            type: 'text',
            name: 'weight',
            label: 'Peso [KG]',
            value: '15'
        },
        {
            type: 'text',
            name: 'consumption',
            label: 'Consumo [KG]',
            value: '15'
        },
        {
            type: 'text',
            name: 'Ganancia',
            label: 'Ganancia [KG]',
            value: '2'
        },
        {
            type: 'text',
            name: 'gad',
            label: 'GAD',
            value: '1'
        },
        {
            type: 'text',
            name: 'convertion',
            label: 'conversion',
            value: '0.0001'
        },
        {
            type: 'text',
            name: 'feed_cost',
            label: 'Costo del alimento',
            value: '100000'
        },
        {
            type: 'text',
            name: 'piglet_meat_price',
            label: 'Precio carne lechón',
            value: '100000'
        },
        {
            type: 'text',
            name: 'income_per_piglet',
            label: 'Ingresos por lechón',
            value: '100000'
        },
        {
            type: 'text',
            name: 'gross_profit',
            label: 'Utilidad bruta',
            value: '100000'
        },
        {
            type: 'text',
            name: 'gross_margin',
            label: 'Margen bruto',
            value: '100000'
        },

    ]
}

const EditDeletePigSectionZootechnicDatesForm = () => {
    return [
        {
            type: 'select',
            name: 'diet',
            label: 'Dieta asignada',
            value: 2537,
            options: [
                {
                    value: 2537,
                    label: 'Dieta 1'
                },
                {
                    value: 2538,
                    label: 'Dieta 2'
                },
                {
                    value: 2539,
                    label: 'Dieta 3'
                }
            ],
        },
        {
            type: 'select',
            name: 'barnyard',
            label: 'Corral',
            value: 2537,
            options: [
                {
                    value: 2537,
                    label: 'Corral 1'
                },
                {
                    value: 2538,
                    label: 'Corral 2'
                },
                {
                    value: 2539,
                    label: 'Corral 3'
                }
            ],
        },
        {
            type: 'select',
            name: 'stage',
            label: 'Etapa',
            value: 2537,
            options: [
                {
                    value: 2537,
                    label: 'Etapa 1'
                },
                {
                    value: 2538,
                    label: 'Etapa 2'
                },
                {
                    value: 2539,
                    label: 'Etapa 3'
                }
            ],
        },
        {
            type: 'text',
            name: 'stages_days',
            label: 'Días en la etapa',
            value: '15'
        },
        {
            type: 'text',
            name: 'weight',
            label: 'Peso [KG]',
            value: '15'
        },
        {
            type: 'text',
            name: 'consumption',
            label: 'Consumo [KG]',
            value: '15'
        },
        {
            type: 'text',
            name: 'Ganancia',
            label: 'Ganancia [KG]',
            value: '2'
        },
        {
            type: 'text',
            name: 'gad',
            label: 'GAD',
            value: '1'
        },
        {
            type: 'text',
            name: 'convertion',
            label: 'conversion',
            value: '0.0001'
        },
        {
            type: 'text',
            name: 'feed_cost',
            label: 'Costo del alimento',
            value: '100000'
        },
        {
            type: 'text',
            name: 'piglet_meat_price',
            label: 'Precio carne lechón',
            value: '100000'
        },
        {
            type: 'text',
            name: 'income_per_piglet',
            label: 'Ingresos por lechón',
            value: '100000'
        },
        {
            type: 'text',
            name: 'gross_profit',
            label: 'Utilidad bruta',
            value: '100000'
        },
        {
            type: 'text',
            name: 'gross_margin',
            label: 'Margen bruto',
            value: '100000'
        }
    ]
}

const UpdatePigSectionInformation = () => {
    return [
        {
            type: 'text',
            name: 'id_mom',
            label: 'ID Madre',
            value: '150'
        },
        {
            type: 'text',
            name: 'gender',
            label: 'Genero',
            value: 'Macho'
        },
        {
            type: 'text',
            name: 'breed',
            label: 'Genetica',
            value: 'LANDRACE'
        }
    ]
}

const EditDeletePigSectionInformation = () => {
    return [
        {
            type: 'text',
            name: 'id_mom',
            label: 'ID Madre',
            value: '150'
        },
        {
            type: 'text',
            name: 'gender',
            label: 'Genero',
            value: 'Macho'
        },
        {
            type: 'text',
            name: 'breed',
            label: 'Genetica',
            value: 'LANDRACE'
        }
    ]
}

const UpdatePigSectionForm = () => {
    return [
        {
            type: 'text',
            name: 'observation',
            label: 'Observación',
            value: '',
            validations: [
                {
                    type: "maxLength",
                    value: 100,
                    message: "El campo debe tener maximo 100 caracteres"
                }
            ]
        },
        {
            type: 'date',
            name: 'update_date',
            label: 'Fecha de actualización',
            value: ''
        }
    ]
}

const EditDeletePigSectionForm = () => {
    return [
        {
            type: 'text',
            name: 'observation',
            label: 'Observación',
            value: 'Nueva observación',
            validations: [
                {
                    type: "maxLength",
                    value: 100,
                    message: "El campo debe tener maximo 100 caracteres"
                }
            ]
        },
        {
            type: 'date',
            name: 'update_date',
            label: 'Fecha de actualización',
            value: '18/08/1998'
        }
    ]
}

const RegisterDietForm = () => {
    return [
        {
            type: 'text',
            name: 'name',
            label: 'Nombre dieta',
            value: '',
            validations: [
                {
                    type: 'required',
                    message: 'El campo es obligatorio'
                }
            ]
        },
        {
            type: 'text',
            name: 'price',
            label: 'Valor en PESOS',
            value: 0,
            validations: [
                {
                    type: 'isNumeric',
                    message: 'El campo debe ser númerico'
                }
            ]
        }
    ]
}

/* Exporting the `forms` object. */
export const forms = {
    login: LoginForm(),
    forgotPasswordLogInKey: ForgotPassworLogInKeyForm(),
    fotgotPassword: ForgotPasswordForm(),
    updatePigSectionInformation: UpdatePigSectionInformation(),
    updatePigSectionZootechnicDates: UpdatePigSectionZootechnicDatesForm(),
    editDeletePigSectionZootechnicDates: EditDeletePigSectionZootechnicDatesForm(),
    editDeletePigSectionInformation: EditDeletePigSectionInformation(),
    editDeletePigSectionForm: EditDeletePigSectionForm(),
    updatePigSectionForm: UpdatePigSectionForm(),
    registerUser: RegisterUserForm(),
    updateUser: UpdateUserForm(),
    updateUserPassword: UpdateUserPasswordForm(),
    newFarm: NewFarmForm(),
    newFarmAdmin: NewFarmFormAdmin(),
    farmAddStage: FarmAddStage(),
    farmAddCauseDeath: FarmAddCauseDeath(),
    farmAddNewness: FarmAddNewness(),
    farmAddBreed: FarmAddBreed(),
    farmAddBarnyard: FarmAddBarnyard(),
    newPigFarm: NewPigForm(),
    newPigBatchForm: NewPigBatchForm(),
    newRawMaterial: NewRawMaterial(),
    registerDietForm: RegisterDietForm()
};
